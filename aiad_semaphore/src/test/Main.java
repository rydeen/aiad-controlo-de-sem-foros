package test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;






import trasmapi.genAPI.Simulator;
import trasmapi.genAPI.TraSMAPI;
import trasmapi.genAPI.exceptions.TimeoutException;
import trasmapi.genAPI.exceptions.UnimplementedMethod;
import trasmapi.sumo.Sumo;
import trasmapi.sumo.SumoLane;
import trasmapi.sumo.SumoTrafficLight;
import trasmapi.sumo.SumoTrafficLightProgram;
import jade.BootProfileImpl;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.ContainerController;
import jade.wrapper.StaleProxyException;

public class Main {
	
	private static ProfileImpl profile;
	private static ContainerController mainContainer;
	
	public static void main(String[] args) throws UnimplementedMethod, InterruptedException, IOException, TimeoutException {	
	    profile = new ProfileImpl();

		Runtime rt = Runtime.instance();
		mainContainer = rt.createMainContainer(profile);
		
		ManagerAgent manager = new ManagerAgent(mainContainer);
		
		try {
			mainContainer.acceptNewAgent("Manager", manager).start();			
		} catch (StaleProxyException e) {
			e.printStackTrace();
			return;
		}
		
		TraSMAPI api = new TraSMAPI(); 

		Simulator sumo = new Sumo("guisim");
		List<String> params = new ArrayList<String>();
		params.add("-c=TlMap/map.sumo.cfg");
		
        try {
            SumoConf conf = SumoConf.load("TlMap/map.sumo.cfg");
            sumo.addParameters(params);
            sumo.addConnections("localhost", conf.getLocalPort());
        } catch (Exception e) {
            e.printStackTrace();
        }

		api.addSimulator(sumo);
		api.launch();
		api.connect();
		api.start();
		
		
		// Waits for SUMO to load. Is enough?
		Thread.sleep(1000);
		
		ArrayList<String> list = SumoTrafficLight.getIdList();
		SumoTrafficLight light = new SumoTrafficLight(list.get(0));
		
		//light.setProgram(light.getProgram());
		
		SumoTrafficLightProgram program = new SumoTrafficLightProgram("myprogram");
		program.addPhase(1000, "rrrrrr");
		program.addPhase(1000, "GGGGGG");
		
		//light.setProgram(program);	
        for (String lane: new SumoTrafficLight(list.get(0)).getControlledLanes()) {
            System.out.println(new SumoLane(lane).getEdgeId());            
        }
        System.out.println("");            
        for (String lane: new SumoTrafficLight(list.get(1)).getControlledLanes()) {
            System.out.println(new SumoLane(lane).getEdgeId());            
        }        
		while(true)
			if(!api.simulationStep(0))
				break;
	}	
}
