package test;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class SumoConf {
    
    /** Path of the configuration file. */
    private String file;
    
    private int localPort;
    
    private SumoConf(String file) {
        this.file = file;
    }

    public static SumoConf load(String file) throws Exception { 
        File                   confFile = new File(file);
        DocumentBuilderFactory factory  = DocumentBuilderFactory.newInstance();
        DocumentBuilder        builder  = factory.newDocumentBuilder();
        Document               document = builder.parse(confFile);
        Element                root     = document.getDocumentElement();
        SumoConf               conf     = new SumoConf(file);
        
        root.normalize();
        NodeList server = root.getElementsByTagName("traci_server");        
        NodeList serverProperties = server.item(0).getChildNodes();
        
        for (int i = 0; i < serverProperties.getLength(); i++) {
            Node node = serverProperties.item(i); 
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                conf.setLocalPort(Integer.valueOf(((Element) node).getAttribute("value")));
            }
        }
                
        return conf;
    }

    public int getLocalPort() {
        return localPort;
    }

    public void setLocalPort(int localPort) {
        this.localPort = localPort;
    }  
}
