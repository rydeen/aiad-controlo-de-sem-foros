package test;

import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.wrapper.ContainerController;

public class ManagerAgent extends Agent {
    
    private ContainerController mainContainer;

    public ManagerAgent(ContainerController mainContainer) {
        this.mainContainer = mainContainer;
    }
    
    protected void setup() {
        DFAgentDescription ad = new DFAgentDescription();
        ad.setName(getAID());

        ServiceDescription sd = new ServiceDescription();
        sd.setName(getName());
        sd.setType("Manager");

        ad.addServices(sd);
        try {
            DFService.register(this, ad);
        } catch(FIPAException e) {
            e.printStackTrace();
        }
        super.setup();
    }


    @Override
    protected void takeDown() {
        try {
            DFService.deregister(this);  
        } catch(FIPAException e) {
            e.printStackTrace();
        }
        super.takeDown();
    }

}
