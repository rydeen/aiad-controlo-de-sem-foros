package main.qlearning;

/**
 * Represents a reward function for a q-learning algorithm.
 */
public interface Reward {
    
    /**
     * Returns the reward of a tuple (state, action) according to the function.
     * 
     * @param state The tuple state.
     * @param action The tuple action.
     * @return The reward.
     */
    public float getValue(State state, Action action);
    
}