package main.qlearning.reward;

import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

import trasmapi.sumo.SumoEdge;

public class VehicleProvider {

    private static VehicleProvider instance;

    private Map<String, Integer> vehicles;
    private List<String> edges;
    
    private VehicleProvider() {
        vehicles = new ConcurrentHashMap<String, Integer>();
        edges = SumoEdge.getEdgeIdList();
        for (String edge: edges) {
            vehicles.put(edge, new Integer(0));
        }
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
        
            @Override
            public void run() {
                for (String edge: edges) {
                    vehicles.put(edge, new SumoEdge(edge).getNumVehicles());
                }
            }
                
        }, 200, 200);
    }
    
    public static int getCount(String edge) {
        return getInstance().vehicles.get(edge);
    }

    private static VehicleProvider getInstance() {
        if (instance == null) {
            instance = new VehicleProvider();
        }
        return instance;
    }
    
}
