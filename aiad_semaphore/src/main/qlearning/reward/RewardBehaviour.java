package main.qlearning.reward;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import trasmapi.sumo.Pair;
import trasmapi.sumo.SumoLane;
import main.agents.TLAgent;
import main.agents.TLNeighbourhood;
import main.qlearning.Action;
import main.qlearning.Reward;
import main.qlearning.State;
import jade.core.AID;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;

/**
 * Represents a TLAgent behaviour that cyclicly handles reward messages. Also
 * implements Reward, making reward queries to neighbours. The total reward is
 * composed by an internal part which must be specified in a subclass and by an
 * external part that is the lane's length weighted average of the reward
 * responses. 
 */
public abstract class RewardBehaviour extends CyclicBehaviour implements Reward {

    private static final long serialVersionUID = -3132184394519162834L;

    /** Queue with the responses to reward queries. */
    private BlockingQueue<Pair<String, Float>> responses;
    
    /** A neighbourhood teller. */
    private TLNeighbourhood neighbourhood;
    
    /** The weight of the internal reward for the total reward. */
    private float internalWeight;
    
    /**
     * Creates a RewardBehavior instance.
     * 
     * @param agent The agent which is going to have this behaviour.
     * @param neighbourhood A neighbourhood teller.
     * @param internalWeight The weight of the internal reward. Between 0 and 1.
     */
    public RewardBehaviour(TLAgent agent, TLNeighbourhood neighbourhood,
            float internalWeight) {
        super(agent);
        this.neighbourhood = neighbourhood;
        this.responses = new LinkedBlockingQueue<Pair<String, Float>>();
    }
    
    /**
     * Returns the reward value during a complete traffic light cycle.
     * 
     * @param requester The agent requesting the value.
     * @return The cycle reward.
     */
    public abstract float getCycleValue(String requester);
    
    /**
     * Sets the weight of the internal reward for the total reward.
     * 
     * @param internalWeight The new weight. Between 0 and 1.
     */
    public void setInternalWeight(float internalWeight) {
        this.internalWeight = internalWeight;
    }

    @Override
    public float getValue(State state, Action action) {
        // Sends queries for reward.
        ACLMessage msg = new ACLMessage(ACLMessage.QUERY_REF);
        List<AID> neighbours = neighbourhood.getNeighbours((TLAgent)getAgent());
        for (AID neighbour: neighbours) {
            msg.addReceiver(neighbour);
        }
        msg.setContent("reward");
        getAgent().send(msg);
        
        // Computes total reward.
        float internal = getCycleValue(((TLAgent)getAgent()).getTrafficLight().
                getId());
        float external = 0.f;
        try {
            float totalLength = 0.f;
            List<Float> rewards = new LinkedList<Float>();
            for (int count = 0; count < neighbours.size(); count++) {
                Pair<String, Float> response = responses.take();
                float length = new SumoLane(response.first()).getLength();
                totalLength += length;
                rewards.add(length * response.second());
            }
            for (Float reward: rewards) {
                external += reward / totalLength;
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return internalWeight * internal + (1.f - internalWeight) * external;
    }
    
    @Override
    public void action() {
        final ACLMessage msg = getAgent().blockingReceive();
        // Handles requests.
        if (msg.getPerformative() == ACLMessage.QUERY_REF) {
            final ACLMessage reply = new ACLMessage(ACLMessage.INFORM_REF);
            reply.addReceiver(msg.getSender());
            // Reward request.
            if (msg.getContent().equals("reward")) {
                new Thread(new Runnable() {
                    
                    @Override
                    public void run() {
                        String requester = TLFromAgent(msg.getSender().
                                getLocalName());
                        Float result = new Float(getCycleValue(requester));
                        reply.setContent(result.toString());
                        getAgent().send(reply);
                    }

                }).start();
            }
        } else if (msg.getPerformative() == ACLMessage.INFORM_REF) {
            try {
                // Puts reward responses available.
                responses.put(new Pair<String, Float>(
                        laneFromAgent(msg.getSender().getLocalName()), 
                        new Float(Float.valueOf(msg.getContent()))));
            } catch (NumberFormatException | InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    
    private String laneFromAgent(String agentName) {
        return agentName.substring(3) + "to" + ((TLAgent) getAgent()).
                getTrafficLight().getId() + "_0";
    }
    
    private String TLFromAgent(String agentName) {
        return agentName.substring(3);
    }
    
}
