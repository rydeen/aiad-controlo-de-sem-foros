package main.qlearning.reward;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import trasmapi.sumo.SumoLane;
import trasmapi.sumo.SumoTrafficLight;
import main.agents.TLAgent;
import main.agents.TLNeighbourhood;

/**
 * Represents a reward that returns the average of vehicles that were in the
 * agent neighbourhood during a traffic light cycle.
 */
public class VehicleAverageReward extends RewardBehaviour {

    private static final long serialVersionUID = -4865564775925312061L;

    /**
     * Represents a request for a cycle reward.
     */
    private class CycleRequest {
        
        /** The id of the traffic light that made the request. */
        private String requester;
        
        /** The time at which the cycle will end. */
        private int time;
        
        /** Samples of vehicles count. */
        private List<Integer> vehicles;
        
        /**
         * Creates a CycleRequest instance.
         * 
         * @param requester The id of the traffic light requesting.
         * @param time The cycle end time.
         */
        public CycleRequest(String requester, int time) {
            this.requester = requester;
            this.time = time;
            this.vehicles = new LinkedList<Integer>();
        }

        public int getTime() {
            return time;
        }
        
        public String getRequester() {
            return requester;
        }
        
        public void addStepVehicles(int count) {
            vehicles.add(count);
        }
        
        public float getResult() {
            int result = 0;
            for (Integer count: vehicles) {
                result += count;
            }
            return result / (vehicles.size() + 1);
        }
        
        public void setTime(int time) {
            this.time = time;
        }
    }
    
    /** Map from the traffic light ids to their responses. */
    private Map<String, BlockingQueue<Float>> queues;
    
    /** Not yet finished cycle requests. */
    private List<CycleRequest> requests;
    
    /** Requests waiting to be handled. */
    private BlockingQueue<CycleRequest> requestsOnWait;
    
    /** Neighbour edges. */
    private Set<String> edges;
    
    /**
     * Creates a VehicleAverageReward instance.
     * 
     * @param agent The agent giving the reward.
     * @param neighbourhood A neighbourhood teller.
     * @param internalWeight The weight of the internal reward. Between 0 and 1.
     */
    public VehicleAverageReward(TLAgent agent, TLNeighbourhood neighbourhood,
            float internalWeight) {
        super(agent, neighbourhood, internalWeight);
        List<String> controlledLanes =
                agent.getTrafficLight().getControlledLanes();
        edges = new HashSet<String>();
        for (String lane: controlledLanes) {
            edges.add(new SumoLane(lane).getEdgeId());
        }
        queues = new HashMap<String, BlockingQueue<Float>>();
        requests = new LinkedList<CycleRequest>();
        requestsOnWait = new LinkedBlockingQueue<CycleRequest>();
    }
    
    /**
     * Must be called on each simulation step to update vehicles in the agent
     * neighbourhood.
     */
    public void onSimulationStep(final int time) {
        while (!requestsOnWait.isEmpty()) {
            try {
                CycleRequest request = requestsOnWait.take();
                request.setTime(time + request.getTime());
                requests.add(request);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        List<CycleRequest> toRemove = new LinkedList<CycleRequest>();
        for (CycleRequest request: requests) {
            if (time >= request.getTime()) {
                try {
                    queues.get(request.getRequester()).put(request.getResult());
                    toRemove.add(request);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        for (CycleRequest request: toRemove) {
            requests.remove(request);
        }
        int sum = 0;
        for (String edge: edges) {
            sum += VehicleProvider.getCount(edge);
        }
        for (CycleRequest request: requests) {
            request.addStepVehicles(sum);
        }
    }
    
    private void addCycleRequest(String requester, int duration) {
        queues.put(requester, new LinkedBlockingQueue<Float>());
        try {
            requestsOnWait.put(new CycleRequest(requester, duration));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public float getCycleValue(String requester) {
        SumoTrafficLight light = ((TLAgent)getAgent()).getTrafficLight();
        addCycleRequest(requester, light.getProgram().getDuration());
        try {
            float value = queues.get(requester).take();
            return value;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return 0.f;
    }

}
