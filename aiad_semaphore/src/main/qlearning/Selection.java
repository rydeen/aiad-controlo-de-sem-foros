package main.qlearning;

/**
 * Represents an action selection method for a q-learning algorithm.
 */
public interface Selection {
    
    /**
     * Returns the action selected for the specified situation.
     * 
     * @param state The current state.
     * @param table The q-table.
     * @return The action selected according to some method.
     */
    public Action getSelection(State state, QTable table);
    
}