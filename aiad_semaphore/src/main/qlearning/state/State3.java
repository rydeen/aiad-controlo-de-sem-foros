package main.qlearning.state;

/**
 * Represents a q-learning state that describes the duration of phases, the
 * traffic intensity level and the day of the week.
 */
public class State3 extends State2 {
    
    /**
     * Day of the week.
     */
    public enum Day {
        SUNDAY,
        MONDAY,
        TUESDAY,
        WEDNESDAY,
        THURSDAY,
        FRIDAY,
        SATURDAY
    }
    
    /** Day of the week. */
    private Day day;
    
    /**
     * Creates a State3 instance.
     */
    public State3() {
        super();
    }
    
    /**
     * Creates a State3 instance from another instance.
     * 
     * @param state The state to be used.
     */
    public State3(State3 state) {
        super(state);
        this.day = state.day;
    }
    
    /**
     * Creates a State3 instance with a specified day of the week.
     * 
     * @param traffic The traffic level.
     * @param day The day of the week.
     */
    public State3(Traffic traffic, Day day) {
        super(traffic);
        this.day = day;
    }

    /**
     * Returns the day of the week.
     * 
     * @return The day of the week.
     */
    public Day getDay() {
        return day;
    }
    
    /**
     * Sets the day of the week.
     * 
     * @param traffic The new day of the week.
     */
    public void setDay(Day day) {
        this.day = day;
    }
    
    @Override
    public boolean equals(Object obj) {
        State3 state = (State3) obj;
        return super.equals(obj) & state.day.equals(day);
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        switch (day) {
        case SUNDAY: hash = 0; break;
        case MONDAY: hash = 1; break;
        case TUESDAY: hash = 2; break;
        case WEDNESDAY: hash = 3; break;
        case THURSDAY: hash = 4; break;
        case FRIDAY: hash = 5; break;
        case SATURDAY: hash = 6; break;
        }
        return hash + (super.hashCode() * 7);
    }
    
}
