package main.qlearning.state;

/**
 * Represents a q-learning state that describes the duration of phases and the
 * traffic intensity level.
 */
public class State2 extends State1 {
    
    /**
     * Traffic intensity levels.
     */
    public enum Traffic {
        LOW,
        MEDIUM,
        HIGH
    }
    
    /** Traffic level. */
    private Traffic traffic;
    
    /**
     * Creates a State2 instance.
     */
    public State2() {
        super();
    }
    
    /**
     * Creates a State2 instance from another instance.
     * 
     * @param state The state to be used.
     */
    public State2(State2 state) {
        super(state);
        this.traffic = state.traffic;
    }
    
    /**
     * Creates a State2 instance with a specified traffic level.
     * 
     * @param traffic The traffic level.
     */
    public State2(Traffic traffic) {
        super();
        this.traffic = traffic;
    }

    /**
     * Returns the traffic intensity level.
     * 
     * @return The traffic level.
     */
    public Traffic getTraffic() {
        return traffic;
    }
    
    /**
     * Sets the traffic intensity level.
     * 
     * @param traffic The new traffic level.
     */
    public void setTraffic(Traffic traffic) {
        this.traffic = traffic;
    }
    
    @Override
    public boolean equals(Object obj) {
        State2 state = (State2) obj;
        return super.equals(obj) & state.traffic.equals(traffic);
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        switch (traffic) {
        case LOW: hash = 0; break;
        case MEDIUM: hash = 1; break;
        case HIGH: hash = 2; break;
        }
        return hash + (super.hashCode() * 3);
    }

}
