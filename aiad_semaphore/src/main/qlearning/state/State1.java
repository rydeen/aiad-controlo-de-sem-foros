package main.qlearning.state;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import main.qlearning.State;

/**
 * Represents a q-learning state that describes the duration of phases. 
 */
public class State1 extends State {
    
    /** Map of phase index to it's duration. */
    private Map<Integer, Integer> durations;
    
    /**
     * Creates a State1 instance.
     */
    public State1() {
        durations = new HashMap<Integer, Integer>();
    }
    
    /**
     * Creates a State1 instance from other instance.
     * 
     * @param state The state to be used.
     */
    public State1(State1 state) {
        durations = new HashMap<Integer, Integer>(state.durations);
    }
    
    /**
     * Returns the duration of the phase with the specified index.
     * 
     * @param index The phase index.
     * @return The phase duration if the phase index exists, null otherwise.
     */
    public int getDuration(int index) {
        return durations.get(index);
    }
    
    /**
     * Sets the duration of the phase with the specified index.
     * 
     * @param index The phase index.
     * @param duration The new duration.
     */
    public void setDuration(int index, int duration) {
        durations.put(index, duration);
    }
    
    /**
     * Returns a collection with the phases indexes stored.
     * 
     * @return The phases indexes.
     */
    public Collection<Integer> getIndexes() {
        return durations.keySet();
    }
    
    @Override
    public boolean equals(Object obj) {
        State1 state = (State1) obj;
        return state.durations.equals(durations);
    }
    
    @Override
    public int hashCode() {
        return durations.hashCode();
    }
    
    @Override
    public String toString() {
        String str = new String();
        for (Integer index: durations.keySet()) {
            str += index + "" + durations.get(index);
        }
        return str;
    }

}
