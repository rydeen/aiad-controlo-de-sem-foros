package main.qlearning.scenario;

import java.util.List;

import main.qlearning.QTable;
import main.qlearning.action.Action1;
import main.qlearning.state.State1;
import trasmapi.sumo.SumoTrafficLight;

/**
 * Represents a scenario in which is used a State1 as scenario state and an
 * Action1 as scenario action.
 */
public class ScenarioA extends DurationScenario {
   
    /**
     * Creates a ScenarioA instance.
     * 
     * @param trafficLight The traffic light to control.
     * @param minimum The minimum phase duration.
     * @param maximum The maximum phase duration.
     * @param granularity The granularity of the phases' duration.
     */
    public ScenarioA(SumoTrafficLight trafficLight, int minimum, int maximum,
            int granularity) {
        super(trafficLight, minimum, maximum, granularity);
    }
    
    @Override
    public State1 getEmptyState() {
        return new State1();
    }

    @Override
    public Action1 getEmptyAction() {
        return new Action1();
    }

    @Override
    public State1 getCopy(State1 state) {
        return new State1(state);
    }

    @Override
    public Action1 getCopy(Action1 action) {
        return new Action1(action);
    }

    @Override
    public void addToTable(QTable table, State1 state, List<Action1> actions) {
        for (Action1 action: actions) {
            table.setValue(state, action);
        }
    }

}
