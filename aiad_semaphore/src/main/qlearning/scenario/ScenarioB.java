package main.qlearning.scenario;

import java.util.List;

import main.qlearning.QTable;
import main.qlearning.action.Action1;
import main.qlearning.state.State1;
import main.qlearning.state.State2;
import main.qlearning.state.State2.Traffic;
import trasmapi.sumo.SumoTrafficLight;

/**
 * Represents a scenario in which is used a State2 as scenario state and an
 * Action1 as scenario action.
 */
public class ScenarioB extends DurationScenario {

    /**
     * Creates a ScenarioB instance.
     * 
     * @param trafficLight The traffic light to control.
     * @param minimum The minimum phase duration.
     * @param maximum The maximum phase duration.
     * @param granularity The granularity of the phases' duration.
     */
    public ScenarioB(SumoTrafficLight trafficLight, int minimum, int maximum,
            int granularity) {
        super(trafficLight, minimum, maximum, granularity);
    }
    
    @Override
    public void onSimulationStep(int currentSimStep) {
        State2 state = (State2) getState();        
        int timeofDay = currentSimStep % 480000;
        Traffic chosen = null;
        if( timeofDay < 150000) {
            chosen = Traffic.LOW;
        } else if( timeofDay < 180000) {
            chosen = Traffic.HIGH;
        } else if( timeofDay < 360000) {
            chosen = Traffic.MEDIUM;
        } else if( timeofDay < 400000) {
            chosen = Traffic.HIGH;
        } else if( timeofDay < 480000) {
            chosen = Traffic.LOW;
        }
        state.setTraffic(chosen);        
        super.onSimulationStep(currentSimStep);
    }
    
    @Override
    public State1 getEmptyState() {
        return new State2();
    }

    @Override
    public Action1 getEmptyAction() {
        return new Action1();
    }

    @Override
    public State1 getCopy(State1 state) {
        return new State2((State2) state);
    }

    @Override
    public Action1 getCopy(Action1 action) {
        return new Action1(action);
    }

    @Override
    public void addToTable(QTable table, State1 state, List<Action1> actions) {
        for (Action1 action: actions) {
            for (State2.Traffic traffic: State2.Traffic.values()) {
                State2 extended = new State2((State2) state);
                extended.setTraffic(traffic);
                table.setValue(extended, action);
            }
        }
    }

}
