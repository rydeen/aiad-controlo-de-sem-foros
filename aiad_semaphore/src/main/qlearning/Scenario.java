package main.qlearning;

/**
 * Represents a q-learning scenario.
 */
public interface Scenario {
    
    /**
     * Returns the current state.
     * 
     * @return The current state.
     */
    public State getState();
    
    /**
     * (Re-)initializes a scenario, returning the correspondent q-table.
     * 
     * @return The initialized q-table.
     */
    public QTable init();

    /**
     * Performs a requested action.
     * 
     * @param action The action requested.
     */
    public void execute(Action action);

}