package main.qlearning;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import trasmapi.sumo.Pair;

/**
 * Represents a table for being used in a q-learning algorithm.
 */
public class QTable {    
    
    /** Map of tuples (state, action) to q-values. */
    private Map<State, Map<Action, Float>> values;
    
    /** Object for generating random values. */
    private Random random;
    
    /**
     * Creates a QTable instance.
     */
    public QTable() {
        values = new HashMap<State, Map<Action, Float>>();
        random = new Random();
    }
    
    /**
     * Sets the q-value of a tuple (state, action) to 0.0f. If the tuple doesn't
     * exist it's inserted in the table.
     * 
     * @param state The state of the tuple.
     * @param action The action of the tuple.
     */
    public void setValue(State state, Action action) {
        this.setValue(state, action, 0.f);
    }

    /**
     * Sets the q-value of a tuple (state, action) to a specified value. If the
     * tuple doesn't exist it's inserted in the table.
     * 
     * @param state The state of the tuple.
     * @param action The action of the tuple.
     * @param value The tuple q-value.
     */
    public void setValue(State state, Action action, float value) {
        if (values.get(state) == null)
            values.put(state, new HashMap<Action, Float>());
        values.get(state).put(action, new Float(value));
    }
    
    /**
     * Returns the q-value of a given tuple (state, action).
     * 
     * @param state The tuple state.
     * @param action The tuple action.
     * @return The tuple q-value or Float.MAX_VALUE if the tuple doesn't exist.
     */
    public float getValue(State state, Action action) {
        Map<Action, Float> actions = values.get(state);
        Float value = actions != null ? actions.get(action) : null;
        return value != null ? value : Float.MAX_VALUE;
    }
    
    /**
     * Returns a state map with it's actions and the correspondent q-value.
     * 
     * @param state The state.
     * @return The requested map if the table has actions for that state, null
     * otherwise.
     */
    public Map<Action, Float> getActions(State state) {
        return values.get(state);
    }
    
    /**
     * Returns a tuple (action, q-value) with the maximum q-value for a given
     * state.
     * 
     * @param state The state.
     * @return The requested tuple (action, q-value) if the table has actions
     * for that state, null otherwise.
     */
    public Pair<Action, Float> getMax(State state) {
        Map<Action, Float> actions = values.get(state);
        if (actions == null || actions.size() == 0)
            return null;
        Float max = Float.MIN_VALUE;
        Action action = null;
        for (Entry<Action, Float> entry: actions.entrySet()) {
            if (entry.getValue() > max) {
                max = entry.getValue();
                action = entry.getKey();
            }
        }
        return new Pair<Action, Float>(action, max);
    }
    
    /**
     * Returns the maximum q-value of given state.
     * 
     * @return The maximum q-value.
     */
    public float getMaxValue(State state) {
        Pair<Action, Float> max = getMax(state);
        return max != null ? max.second() : null;
    }
    
    /**
     * Returns a tuple (action, q-value) with the minimum q-value for a given
     * state.
     * 
     * @param state The state.
     * @return The requested tuple (action, q-value) if the table has actions
     * for the specified state, null otherwise.
     */
    public Pair<Action, Float> getMin(State state) {
        Map<Action, Float> actions = values.get(state);
        if (actions == null || actions.size() == 0)
            return null;
        Float min = Float.MAX_VALUE;
        Action action = null;
        for (Entry<Action, Float> entry: actions.entrySet()) {
            if (entry.getValue() < min) {
                min = entry.getValue();
                action = entry.getKey();
            }
        }
        return new Pair<Action, Float>(action, min);
    }
    
    public List<Pair<Action, Float>> getMins(State state) {
        List<Pair<Action, Float>> mins = new LinkedList<Pair<Action, Float>>();
        Map<Action, Float> actions = values.get(state);
        if (actions == null || actions.size() == 0)
            return null;
        Float min = Float.MAX_VALUE;
        for (Entry<Action, Float> entry: actions.entrySet()) {
            if (entry.getValue() < min) {
                mins.clear();
                mins.add(new Pair<Action, Float>(entry.getKey(), min));
                min = entry.getValue();
            } else if (entry.getValue() == min) {
                mins.add(new Pair<Action, Float>(entry.getKey(), min));
            }
        }
        return mins;
    }
    
    /**
     * Returns the minimum q-value of given state.
     * 
     * @return The minimum q-value.
     */
    public float getMinValue(State state) {
        Pair<Action, Float> min = getMax(state);
        return min != null ? min.second() : null;
    }
    
    /**
     * Returns a random action of a given state.
     * 
     * @return An action if the table has actions for the specified state, null
     * otherwise.
     */
    public Action getRandom(State state) {
        Map<Action, Float> actions = values.get(state);
        if (actions == null || actions.size() == 0)
            return null;
        Iterator<Entry<Action, Float>> it = actions.entrySet().iterator();
        for (int i = 0; i < random.nextInt(actions.size()); i++) {
            it.next();
        }
        return it.hasNext() ? ((Entry<Action, Float>) it.next()).getKey() :
            null;        
    }

}