package main.qlearning.selection;

import java.util.Random;

import main.qlearning.Action;
import main.qlearning.QTable;
import main.qlearning.Selection;
import main.qlearning.State;

/**
 * Represents the e-greedy action selection method. The probability of not
 * selecting the best action is e.
 */
public class EGreedySelection implements Selection {

    /** The probability/e-value. */
    private float probability;
    
    /** Object for generating random values. */
    private Random random;
    
    /**
     * Creates a EGreedySelection instance.
     * 
     * @param probability The probability/e-value to be used.
     */
    public EGreedySelection(float probability) {
        this.probability = probability;
        this.random = new Random();
    }
    
    /**
     * Sets the probability/e-value.
     * 
     * @param probability The new probability/e-value.
     */
    public void setProbability(float probability) {
        this.probability = probability;
    }
    
    @Override
    public Action getSelection(State state, QTable table) {
        if (random.nextFloat() <= probability) {
            return table.getRandom(state);
        }
        return table.getMin(state).first();
    }
    
}