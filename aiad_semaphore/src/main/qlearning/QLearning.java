package main.qlearning;

/**
 * Implements a generic q-learning algorithm.
 */
public class QLearning {
    
    /** Q-learning scenario. */
    private Scenario scenario;
    
    /** Algorithm q-table. */
    private QTable table;
    
    /** Reward function. */
    private Reward reward;
    
    /** Action selection method. */
    private Selection selection;
    
    /** Learning rate. */
    private float learningRate;
    
    /** Discount factor. */
    private float discountFactor;
    
    /**
     * Creates a QLearning instance.
     * 
     * @param scenario The Q-learning scenario.
     * @param reward The reward function.
     * @param selection The action selection method.
     * @param lr The learning rate.
     * @param df The discount factor.
     */
    public QLearning(Scenario scenario, Reward reward, Selection selection,
            float lr, float df) {
        this.scenario = scenario;
        this.reward = reward;
        this.selection = selection;
        this.learningRate = lr;
        this.discountFactor = df;
        this.table = scenario.init();
    }
    
    /**
     * Applies a q-learning step.
     */
    public void step() {
        State old = scenario.getState();
        
        Action action = selection.getSelection(scenario.getState(), table);
        scenario.execute(action);
        
        float value = (1.0f - learningRate) * table.getValue(old, action) +
                learningRate * (reward.getValue(old, action) + discountFactor *
                        table.getMaxValue(scenario.getState()));
        table.setValue(old, action, value);
    }
    
    /**
     * Sets the reward function.
     * 
     * @param reward The new reward function.
     */
    public void setReward(Reward reward) {
        this.reward = reward;
    }
    
    /**
     * Sets the action selection method.
     * 
     * @param selection The new action selection method.
     */
    public void setSelection(Selection selection) {
        this.selection = selection;
    }
    
    /**
     * Sets the learning rate.
     * 
     * @param learningRate The new learning rate.
     */
    public void setLearningRate(float learningRate) {
        this.learningRate = learningRate;
    }
    
    /**
     * Sets the discount factor.
     * 
     * @param discountFactor The new discount factor.
     */
    public void setDiscountFactor(float discountFactor) {
        this.discountFactor = discountFactor;
    }
    
}