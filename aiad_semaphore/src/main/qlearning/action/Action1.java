package main.qlearning.action;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import main.qlearning.Action;

/**
 * Represents a q-learning action that indicates what to do with each phase
 * duration.
 */
public class Action1 extends Action {

    /** Map of phase index to phase duration action. */
    private Map<Integer, DurationAction> actions;
    
    /**
     * Creates a Action1 instance.
     */
    public Action1() {
        this.actions = new HashMap<Integer, DurationAction>();
    }
    
    /**
     * Creates a Action1 instance from other instance.
     * 
     * @param action The action to be used.
     */
    public Action1(Action1 action) {
        actions = new HashMap<Integer, DurationAction>(action.actions);
    }
    
    /**
     * Returns the duration action of the phase with the specified index.
     * 
     * @param index The phase index.
     * @return The phase duration action if the phase index exists, null
     * otherwise.
     */
    public DurationAction getDuractionAction(int index) {
        return actions.get(index);
    }
    
    /**
     * Sets the duration action of the phase with the specified index.
     * 
     * @param index The phase index.
     * @param action The phase duration action.
     */
    public void setDurationAction(int index, DurationAction action) {
        actions.put(index, action);
    }
    
    /**
     * Returns a collection with the phases indexes stored.
     * 
     * @return The phases indexes.
     */
    public Collection<Integer> getIndexes() {
        return actions.keySet();
    }
    
    @Override
    public boolean equals(Object obj) {
        Action1 action = (Action1) obj;
        return action.actions.equals(actions);
    }
    
    @Override
    public int hashCode() {
        return actions.hashCode();
    }
    
    @Override
    public String toString() {
        String str = new String();
        for (Integer index: actions.keySet()) {
            str += index + actions.get(index).toString();
        }
        return str;
    }
    
}
