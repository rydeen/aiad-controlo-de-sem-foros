package main.qlearning.action;

/**
 * Phase duration actions. 
 */
public enum DurationAction {
    INCREASE,
    MAINTAIN,
    DECREASE
}