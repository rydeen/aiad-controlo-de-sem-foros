package main.agents;

import jade.core.AID;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import trasmapi.sumo.SumoLane;

/**
 * Represents the neighbourhood of a traffic light agent that returns only the
 * direct adjacent agents.
 */
public class AdjacentNeighbourhood implements TLNeighbourhood {
    
    private Map<String, List<AID>> neighbours;
    
    public AdjacentNeighbourhood() {
        neighbours = new HashMap<String, List<AID>>();
    }
    
    @Override
    public List<AID> getNeighbours(TLAgent agent) {
        List<AID> ids = neighbours.get(agent.getTrafficLight().getId());
        if (ids != null) {
            return ids;
        }
        ids = new LinkedList<AID>();
        Set<String> names = new HashSet<String>();
        List<String> controlledLanes =
                agent.getTrafficLight().getControlledLanes();
        for (String lane: controlledLanes) {
            names.add(edgeToTL(new SumoLane(lane).getEdgeId()));
        }
        for (String name: names) {
            DFAgentDescription[] result = null;
            DFAgentDescription template = new DFAgentDescription();
            ServiceDescription sd1 = new ServiceDescription();
            sd1.setType(name);
            template.addServices(sd1);
            try {
                result = DFService.search(agent, template);
            } catch (FIPAException e) {
            }
            if (result != null && result.length > 0)
                ids.add(result[0].getName());                
        }
        neighbours.put(agent.getTrafficLight().getId(), ids);
        return ids;
    }
    
    private String edgeToTL(String edgeId) {
        int index = edgeId.indexOf("to");
        return edgeId.substring(0, index);
    }

}
