package main.agents;

/**
 * Represents an agent that doesn't change the traffic light plan.
 */
public class FixedTLAgent extends TLAgent {
    
    private static final long serialVersionUID = -5475346228556537843L;

    /**
     * Creates a FixedTLAgent instance.
     * 
     * @param tlId The traffic light id.
     */
    public FixedTLAgent(String tlId) {
		super(tlId);
	}

}
