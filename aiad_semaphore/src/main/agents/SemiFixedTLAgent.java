package main.agents;

import main.qlearning.state.State2.Traffic;

/**
 * Represents an agent that changes the traffic light plan according to time of
 * the day, in a loop.
 */
public class SemiFixedTLAgent extends TLAgent 
{
	private static final long serialVersionUID = 6095960260125307076L;
	private Traffic lastTraffic;

	/**
	 * Creates a SemiFixedTLAgent instance.
	 * 
	 * @param tlId The traffic light id.
	 */
	public SemiFixedTLAgent(String tlId) 
	{
	    super(tlId);
	    lastTraffic = Traffic.LOW;
	    light.setProgram(lastTraffic.name().toLowerCase());
	}

	@Override
	public void onSimulationStep(int currentSimStep)
	{
	    int timeofDay = currentSimStep % 480000;
	    Traffic chosen = null;
	    if( timeofDay < 150000) chosen = Traffic.LOW;
	    else if( timeofDay < 180000) chosen = Traffic.HIGH;
	    else if( timeofDay < 360000) chosen = Traffic.MEDIUM;
	    else if( timeofDay < 400000) chosen = Traffic.HIGH;
	    else if( timeofDay < 480000) chosen = Traffic.LOW;

	    if(chosen != lastTraffic)
	    {
	        light.setProgram(chosen.name().toLowerCase());
	        lastTraffic = chosen;
	    }
	}
}
