package main.agents;

import trasmapi.sumo.SumoTrafficLight;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;

/**
 * Represents an agent that controls a traffic light.
 */
public abstract class TLAgent extends Agent {
    
	private static final long serialVersionUID = 6095960260125307076L;
	
	/** The traffic light that the agent controls. */
	protected SumoTrafficLight light;

	/**
	 * Creates a TLAgent instance.
	 * 
	 * @param tlId The traffic light id.
	 */
	public TLAgent(String tlId) {
		super();
		light = new SumoTrafficLight(tlId);
	}
	
	/**
	 * Action to be made on each simulation step. None by default.
	 * @param currentSimStep 
	 */
	public void onSimulationStep(int currentSimStep) {
	}
	
	/**
	 * Returns the controlled traffic light.
	 * 
	 * @return The controlled traffic light.
	 */
	public SumoTrafficLight getTrafficLight() {
        return light;
    }

	@Override
	protected void setup() {
		DFAgentDescription ad = new DFAgentDescription();
		ad.setName(getAID());

		ServiceDescription sd = new ServiceDescription();
		sd.setName(getName());

		sd.setType(light.getId());
		ad.addServices(sd); 

		try {
			DFService.register(this, ad);
		} catch(FIPAException e) {
			e.printStackTrace();
		}
		
		super.setup();
	}

	@Override
	protected void takeDown() {
		try {
			DFService.deregister(this);  
		} catch(FIPAException e) {
			e.printStackTrace();
		}
		super.takeDown();
	}

}
