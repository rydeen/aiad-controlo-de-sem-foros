package main.agents;

import jade.core.AID;

import java.util.List;

/**
 * Represents the neighbourhood of a traffic light agent.
 */
public interface TLNeighbourhood {
    
    /**
     * Returns a list with the agent identifiers of all neighbours of a
     * specified agent.
     * 
     * @param agent The agent.
     * @return The neighbours.
     */
    public List<AID> getNeighbours(TLAgent agent);

}