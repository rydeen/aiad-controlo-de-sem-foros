package main.statistics;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTable;

import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;

public class StatisticsGUI
{
    private JFrame guiFrame;
    private List<StatisticalVehicle> vehicles;
    private JTable table;
    
    public StatisticsGUI()
    {   
        guiFrame = new JFrame();
        guiFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        guiFrame.setTitle("SUMO Statistics Support");
        guiFrame.setSize(650,630);
        guiFrame.setLocationRelativeTo(null);        
    }
    
    public void setVisible(boolean visible)
    {
        guiFrame.setVisible(visible);
    }
    
    @SuppressWarnings("serial")
    public void setVehicles(List<StatisticalVehicle>  vList)
    {
        DefaultTableModel model = new DefaultTableModel(
                new String[]{"Vehicle Name", "Velocity (m/s)", "Active", 
                             "Mean Time Waiting (s)", "Total Travel Time (s)"}, 
                vList.size());
        table = new JTable(model){@Override
            public boolean isCellEditable(int arg0, int arg1) {

            return false;
        }};

        JScrollPane pane = new JScrollPane(table);
        pane.setBounds(5, 5, 640, 550);
        
        guiFrame.getContentPane().setLayout(null);
        guiFrame.getContentPane().add(pane);

        JButton btnGenerateXls = new JButton("Generate XLS");
        btnGenerateXls.setBounds(224, 570, 185, 25);
        guiFrame.getContentPane().add(btnGenerateXls);
        btnGenerateXls.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) 
            {
                XLSGenerator.generate(vehicles);
            }
        });

        vehicles = vList;
        for(int i = 0; i < vehicles.size(); i++)
        {
            StatisticalVehicle vehicle = vehicles.get(i);
            vehicle.row = i;
            vehicle.id  = vehicle.vehicle.id;
            table.setValueAt(vehicle.vehicle.id, i, 0);
            table.setValueAt("0", i, 1);
            table.setValueAt(vehicle.vehicle.alive ? "Yes" : "No", i, 2);
            table.setValueAt("0", i, 3);
            table.setValueAt("0", i, 4);
        }
    }

    public void updateView(int currentStep)
    {
        for (StatisticalVehicle vehicle : vehicles)
        {
            if(vehicle.travelTime == 0 && vehicle.vehicle.lanePosition > 0)
            {
                table.setValueAt(vehicle.vehicle.speed, vehicle.row, 1);
                table.setValueAt(vehicle.vehicle.alive ? "Yes" : "No", vehicle.row, 2);
                if(vehicle.vehicle.speed == 0.0 && vehicle.lastStepStopped == 0)
                    vehicle.lastStepStopped = currentStep;
                else if(vehicle.vehicle.speed > 0 && vehicle.lastStepStopped > 0)
                {
                    vehicle.meanWaitingTime = (vehicle.meanWaitingTime * vehicle.stoppedTimes + 
                                               currentStep - vehicle.lastStepStopped) 
                                              / ++vehicle.stoppedTimes;
                    vehicle.lastStepStopped = 0;   
                }
                table.setValueAt(vehicle.meanWaitingTime / 1000f, vehicle.row, 3);
                
                if(vehicle.vehicle.arrived)
                {
                    vehicle.travelTime = vehicle.vehicle.arrivalTime - vehicle.vehicle.departTime;
                    table.setValueAt(vehicle.travelTime/1000f,
                                     vehicle.row, 
                                     4);
                    vehicle.vehicle = null;
                }
            }
        }
    }
}
