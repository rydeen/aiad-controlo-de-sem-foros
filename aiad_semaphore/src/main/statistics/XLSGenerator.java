package main.statistics;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class XLSGenerator
{
    public static void generate(List<StatisticalVehicle> vehicles)
    {
        try
        {
            String filename       = "./stats/" + (new Date()).getTime() + ".xls";
            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet sheet       = workbook.createSheet("Data");

            HSSFRow rowhead = sheet.createRow(0);
            rowhead.createCell(0).setCellValue("Vehicle ID");
            rowhead.createCell(1).setCellValue("Mean Time Waiting (ms)");
            rowhead.createCell(2).setCellValue("Total Travel Time (ms)");
            
            for(int i = 0; i < vehicles.size(); i++)
            {
                StatisticalVehicle vehicle = vehicles.get(i);
                HSSFRow row = sheet.createRow(i+1);
                row.createCell(0).setCellValue(vehicle.id);
                row.createCell(1).setCellValue(vehicle.meanWaitingTime);
                row.createCell(2).setCellValue(vehicle.travelTime);
            }
            
            //Mean of Mean Time Waiting global
            HSSFRow r1 = sheet.getRow(1);
            r1.createCell(3).setCellValue("Mean of Mean Time Waiting:");
            r1.createCell(4).setCellFormula("AVERAGE(B2:B"+(vehicles.size()+1)+")");
            //Total Travel Time global
            HSSFRow r2 = sheet.getRow(2);
            r2.createCell(3).setCellValue("Mean of Total Travel Time:");
            r2.createCell(4).setCellFormula("AVERAGE(C2:C"+(vehicles.size()+1)+")");
            //Deviation of Mean Time Waiting
            HSSFRow r3 = sheet.getRow(3);
            r3.createCell(3).setCellValue("Deviation of Mean Time Waiting:");
            r3.createCell(4).setCellFormula("STDEV(B2:B"+(vehicles.size()+1)+")");
            //Deviation of Total Travel Time global
            HSSFRow r4 = sheet.getRow(4);
            r4.createCell(3).setCellValue("Deviation of Total Travel Time:");
            r4.createCell(4).setCellFormula("STDEV(C2:C"+(vehicles.size()+1)+")");
            
            File yourFile = new File(filename);
            if (!yourFile.exists())
                yourFile.createNewFile();

            FileOutputStream fileOut = new FileOutputStream(yourFile, false);
            workbook.write(fileOut);
            fileOut.close();

        } catch (Exception e)
        {
            e.printStackTrace();

        }
    }
}
