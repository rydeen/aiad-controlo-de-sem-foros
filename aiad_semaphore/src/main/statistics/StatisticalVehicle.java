package main.statistics;

import trasmapi.sumo.SumoVehicle;

public class StatisticalVehicle
{
    public float       meanWaitingTime = 0;
    public int         stoppedTimes = 0;
    public int         lastStepStopped = 0;
    public int         travelTime = 0;
    public String      id;
    public SumoVehicle vehicle;
    public int         row;
}
