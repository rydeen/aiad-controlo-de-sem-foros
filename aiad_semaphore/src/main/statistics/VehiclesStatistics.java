package main.statistics;

import java.util.LinkedList;
import java.util.List;

import trasmapi.sumo.SumoVehicle;

public class VehiclesStatistics
{
    private List<StatisticalVehicle> vehicles;
    private StatisticsGUI gui;
    
    public VehiclesStatistics()
    {
        vehicles   = new LinkedList<StatisticalVehicle>();
        gui        = new StatisticsGUI();
    }
    
    public void addVehicle(SumoVehicle vehicle)
    {
        StatisticalVehicle v = new StatisticalVehicle();
        v.vehicle = vehicle;
        vehicles.add(v);
    }

    public void startStatistics()
    {
        gui.setVehicles(vehicles);
        gui.setVisible(true);
    }

    public void updateView(int currentStep)
    {
        gui.updateView(currentStep);
    }
    

}
