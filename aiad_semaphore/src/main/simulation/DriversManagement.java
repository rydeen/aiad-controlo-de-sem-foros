package main.simulation;

import java.util.LinkedList;
import java.util.Random;

import trasmapi.sumo.SumoCom;
import trasmapi.sumo.SumoVehicle;

/**
 * Management of vehicles in SUMO.
 */
public class DriversManagement {
    
    private int numDrivers = 0;
    private Random rand = new Random(25678234);
    
    private final int LOW_NUM_DRIVERS = 1;
    private final int MED_NUM_DRIVERS = 3;
    private final int HIG_NUM_DRIVERS = 7;
    
    private final int NUM_DIAS = 1;
    
    private final int STEP_INC = 30000;
    
    private String[] originList = {"0/3to1/3", "1/3to2/3", "2/3to3/3", "3/3to3/2", "3/2to3/1", "3/1to3/0",
    		                       "3/0to2/0", "2/0to1/0", "1/0to0/0", "0/0to0/1", "0/1to0/2", "0/2to1/2",
    		                       "1/2to2/2", "2/2to2/1", "2/1to1/1"};
    
    private String[] destinList = {"1/0to0/0", "0/0to0/1", "0/1to0/2", "0/2to1/2","1/2to2/2", "2/2to2/1", 
    		                       "0/3to1/3", "1/3to2/3", "2/3to3/3", "3/3to3/2", "3/2to3/1", "3/1to3/0",
                                   "3/0to2/0", "2/0to1/0", "2/1to1/1"};
    
    private String[] mediumOL = {"0/2to1/2", "0/1to1/1", "3/2to2/2", "3/1to2/1"};
    private String[] mediumDL = {"3/2to3/1", "3/1to3/2", "0/1to0/0", "0/2to0/3"};
    
    public LinkedList<SumoVehicle> addDrivers() {
        SumoCom.createAllRoutes();
        LinkedList<SumoVehicle> vehicles = new LinkedList<SumoVehicle>();

        int j = 0;
        int indexOrig = 0;
        int indexDest = 0;
        for(int dia = 0; dia < NUM_DIAS; dia++){
        	for(int step = 0; step < 480000; step += STEP_INC){
        		numDrivers = getNumDrivers(step);
        		
        		for(int i = 0; i < numDrivers; i++) {
        			if(numDrivers != MED_NUM_DRIVERS){
        				indexOrig = rand.nextInt(15);
        				indexDest = rand.nextInt(15);
        			}
        			else{
        				indexOrig = indexDest = rand.nextInt(4);
        			}
        			
        			String origin = getOrigin(indexOrig, numDrivers);
        			String vehicleType = SumoCom.vehicleTypesIDs.get(rand.nextInt(SumoCom.vehicleTypesIDs.size()));
        			String routeId = SumoCom.getRouteId(origin, null);
        			String destination = getDestination(indexDest, numDrivers);

        			if(origin.equals(destination)){
        				int index = 0;
        				if(indexDest == 0) index = 3;
        				
        				destination = getDestination(index, numDrivers);
        			}

        			SumoVehicle vehicle = new SumoVehicle(j++, vehicleType, routeId, step+(480000*dia), 0, 0, (byte) 0);
        			SumoCom.addVehicle(vehicle);
        			SumoCom.addVehicleToSimulation(vehicle);
        			vehicle.changeTarget(destination);
        			
        			vehicles.add(vehicle);
        		}
        	}
        }
        	
        return vehicles;
    }
    
    private int getNumDrivers(int step){
    	return step < 150000 ? LOW_NUM_DRIVERS :
    		   step < 180000 ? HIG_NUM_DRIVERS :
    		   step < 360000 ? MED_NUM_DRIVERS :
    		   step < 400000 ? HIG_NUM_DRIVERS : 
    			               LOW_NUM_DRIVERS;
    }
    
    private String getOrigin(int index, int numDrivers){
    	if(numDrivers == MED_NUM_DRIVERS){
    		return mediumOL[index];
    	}
    	else{
    		return originList[index];
    	}
    }
    
    private String getDestination(int index, int numDrivers){
    	if(numDrivers == MED_NUM_DRIVERS){
    		return mediumDL[index];
    	}
    	else{
    		return destinList[index];
    	}
    }

}
