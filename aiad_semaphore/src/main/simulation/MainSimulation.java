package main.simulation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import main.agents.FixedTLAgent;
import main.agents.LearningTLAgent;
import main.agents.SemiFixedTLAgent;
import main.agents.TLAgent;
import main.statistics.VehiclesStatistics;
import trasmapi.genAPI.*;
import trasmapi.genAPI.exceptions.*;
import trasmapi.sumo.Sumo;
import trasmapi.sumo.SumoCom;
import trasmapi.sumo.SumoTrafficLight;
import trasmapi.sumo.SumoVehicle;
import jade.BootProfileImpl;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;
import jade.wrapper.StaleProxyException;

/**
 * The main simulation of the application.
 */
public class MainSimulation {
    
    private enum Mode { FIXED, SEMI_FIXED, LEARNING };
    private static Mode simulationMode = Mode.LEARNING;
        
    public static void main(String[] args) throws IOException, TimeoutException,
            UnimplementedMethod, InterruptedException, StaleProxyException {
        // Jade initialization.
        List<String> params2 = new ArrayList<String>();
        params2.add("-gui");
        ProfileImpl profile = new BootProfileImpl(params2.toArray(new String[0]));
        ContainerController mainContainer =
                Runtime.instance().createMainContainer(profile);
        
        // Sumo initialization.
        Sumo sumo = new Sumo("guisim");
        List<String> params = new ArrayList<String>();
        params.add("-c=TlMap/map.sumo.cfg");
        
        try {
            SumoConfig conf = SumoConfig.load("TlMap/map.sumo.cfg");
            sumo.addParameters(params);
            sumo.addConnections("localhost", conf.getLocalPort());
        } catch (Exception e) {
            e.printStackTrace();
        }

        // TraSMAPI initialization.
        final TraSMAPI api = new TraSMAPI(); 
        api.addSimulator(sumo);
        api.launch();
        api.connect();
        api.start();        
        
        // Waits for SUMO to load. Is enough?
        Thread.sleep(1000);
        
        // Consults traffic lights and creates an agent for each one.
        ArrayList<String> ids = SumoTrafficLight.getIdList();
        final List<TLAgent> agents = new LinkedList<TLAgent>();
        List<AgentController> controllers = new LinkedList<AgentController>();
        
        System.out.print("Initiating agents ");
        for (int i = 0; i < ids.size(); i++) {
            TLAgent agent;
            if(simulationMode == Mode.LEARNING)
                agent = new LearningTLAgent(ids.get(i));
            else if(simulationMode == Mode.SEMI_FIXED)
                agent = new SemiFixedTLAgent(ids.get(i));
            else
                agent = new FixedTLAgent(ids.get(i));
            
            agents.add(agent);
            controllers.add(mainContainer.acceptNewAgent("TL#" + ids.get(i),
                    agent));
            System.out.print(".");
        }
        System.out.println("");

        // Adds drivers to the simulation and subscribe them
        VehiclesStatistics statistics    = new VehiclesStatistics();
        DriversManagement driversManager = new DriversManagement();
        List<SumoVehicle> vehicles       = driversManager.addDrivers();
        for (SumoVehicle vehicle : vehicles)
        {
            SumoCom.subscribeVehicle(vehicle.id);
            statistics.addVehicle(vehicle);
        }
        vehicles = null;
        
        statistics.startStatistics();
        
        // Starts agents.
        for (AgentController controller: controllers) {
            controller.start();
        }
        
        while(true)
        {
            int currentStep = SumoCom.getCurrentSimStep();
            if(!api.simulationStep(0))
                break;
            for (TLAgent agent: agents)
                agent.onSimulationStep(currentStep);
            statistics.updateView(currentStep);
        }
    }

}
