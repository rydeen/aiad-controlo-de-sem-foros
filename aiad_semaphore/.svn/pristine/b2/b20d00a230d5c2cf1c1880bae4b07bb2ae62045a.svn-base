package main.agents;

import java.util.ArrayList;

import trasmapi.sumo.SumoTrafficLight;

import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.wrapper.ContainerController;
import jade.wrapper.StaleProxyException;

/**
 * 
 * General manager for traffic lights control, with SUMO/JADE
 * 
 * Copyright (C) AIAD @ FEUP project authors. All rights reserved.
 * 
 * @author <b>Filipe Oliveira</b>, <b>Paulo Araújo</b> and <b>Tiago Azevedo</b>
 * 
 */
public class TLManager extends Agent
{
	private static final long serialVersionUID = 1L;
	private ContainerController mainContainer;

	public TLManager(ContainerController mainContainer)
	{
		this.mainContainer = mainContainer;
	}


	protected void setup() 
	{
		DFAgentDescription ad = new DFAgentDescription();
		ad.setName(getAID());

		ServiceDescription sd = new ServiceDescription();
		sd.setName(getName());

		sd.setType("Manager");
		ad.addServices(sd); 

		try 
		{
			DFService.register(this, ad);
		} catch(FIPAException e) 
		{
			e.printStackTrace();
		}

		super.setup();
	}


	@Override
	protected void takeDown() 
	{
		try 
		{
			DFService.deregister(this);  
		} catch(FIPAException e) 
		{
			e.printStackTrace();
		}
		super.takeDown();
	}

	public void loadFixedTls() throws StaleProxyException
	{
		ArrayList<String> tlsIds = new ArrayList<String>();

		tlsIds = SumoTrafficLight.getIdList();

		for(int i = 0; i < tlsIds.size(); i++)
		    mainContainer.acceptNewAgent("TL#"+tlsIds.get(i), new FixedTLAgent(tlsIds.get(i)));
	}
}
