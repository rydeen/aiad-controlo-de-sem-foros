package main.qlearning.scenario;

import java.util.List;

import main.qlearning.QTable;
import main.qlearning.action.Action1;
import main.qlearning.state.State1;
import main.qlearning.state.State2;
import main.qlearning.state.State3;
import trasmapi.sumo.SumoTrafficLight;

/**
 * Represents a scenario in which is used a State3 as scenario state and an
 * Action1 as scenario action.
 */
public class ScenarioC extends DurationScenario {

    /**
     * Creates a ScenarioC instance.
     * 
     * @param trafficLight The traffic light to control.
     * @param minimum The minimum phase duration.
     * @param maximum The maximum phase duration.
     * @param granularity The granularity of the phases' duration.
     */
    public ScenarioC(SumoTrafficLight trafficLight, int minimum, int maximum,
            int granularity) {
        super(trafficLight, minimum, maximum, granularity);
    }

    @Override
    public State1 getEmptyState() {
        return new State3();
    }

    @Override
    public Action1 getEmptyAction() {
        return new Action1();
    }
    
    @Override
    public State1 getCopy(State1 state) {
        return new State3((State3) state);
    }

    @Override
    public Action1 getCopy(Action1 action) {
        return new Action1(action);
    }

    @Override
    public void addToTable(QTable table, State1 state, List<Action1> actions) {
        for (Action1 action: actions) {
            for (State2.Traffic traffic: State2.Traffic.values()) {
                for (State3.Day day: State3.Day.values()) {
                    State3 extended = new State3((State3) state);
                    extended.setTraffic(traffic);
                    extended.setDay(day);
                    table.setValue(extended, action);
                }
            }
        }        
    }

}
