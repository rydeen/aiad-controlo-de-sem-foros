package main.agents;

import main.qlearning.QLearning;
import main.qlearning.Scenario;
import main.qlearning.reward.VehicleAverageReward;
import main.qlearning.scenario.ScenarioA;
import main.qlearning.selection.EGreedySelection;

/**
 * Represents an agent that changes the traffic light plan using coordination
 * strategies.
 */
public class LearningTLAgent extends TLAgent {
    
	private static final long serialVersionUID = 6095960260125307076L;

	private static final int minimumDuration = 30000;
	private static final int maximumDuration = 35000;
	private static final int durationGranularity = 5000;
	
	private static final float learningRate = 0.5f;
	private static final float discountFactor = 0.5f;
	
	private static final float internalWeight = 0.5f;
	
	/** Algorithm used for coordination. */
    private QLearning qlearning;
    
    /** Reward used in q-learning. */
    private VehicleAverageReward reward;
    
    /**
     * Creates a LearningTLAgent instance.
     * 
     * @param tlId The traffic light id.
     */
	public LearningTLAgent(String tlId) {
		super(tlId);
		reward = new VehicleAverageReward(this, new AdjacentNeighbourhood(),
		        internalWeight);
        Scenario scenario = new ScenarioA(getTrafficLight(), minimumDuration,
		        maximumDuration, durationGranularity);
		qlearning = new QLearning(scenario, reward, new EGreedySelection(0.f),
                learningRate, discountFactor);
	}
	
    @Override
	protected void setup() {
	    super.setup();
        addBehaviour(reward);
        new Thread(new Runnable() {
            
            @Override
            public void run() {
                while (true) {
                    qlearning.step();
                    System.out.println("Step: " + getTrafficLight().getId());
                }
            }
            
        }).start();
	}
	
	@Override
	public void onSimulationStep(int currentSimStep) {
	    reward.onSimulationStep(currentSimStep);
	}

}
