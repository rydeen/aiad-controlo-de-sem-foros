package main.qlearning.scenario;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

import trasmapi.sumo.SumoTrafficLight;
import trasmapi.sumo.SumoTrafficLightProgram;
import main.qlearning.Action;
import main.qlearning.QTable;
import main.qlearning.Scenario;
import main.qlearning.State;
import main.qlearning.action.Action1;
import main.qlearning.action.DurationAction;
import main.qlearning.state.State1;

/**
 * Represents a traffic light scenario in which the phases' duration can vary
 * between specified values.
 */
public abstract class DurationScenario implements Scenario {
    
    /** Traffic light being controlled. */
    private SumoTrafficLight trafficLight;
    
    /** The minimum duration of a phase. */
    private int minimumDuration;
    
    /** The maximum duration of a phase. */
    private int maximumDuration;
    
    /** The granularity of the phases' duration. */
    private int durationGranularity;
    
    /** The scenario q-table. */
    private QTable table;
    
    /** The scenario state. */
    private State1 state;

    /**
     * Creates a DurationScenario instance.
     * 
     * @param trafficLight The traffic light to control.
     * @param minimum The minimum duration of a phase.
     * @param maximum The maximum duration of a phase.
     * @param granularity The granularity of the phases' duration. 
     */
    public DurationScenario(SumoTrafficLight trafficLight, int minimum,
            int maximum, int granularity) {
        this.trafficLight = trafficLight;
        this.minimumDuration = minimum;
        this.maximumDuration = maximum;
        this.durationGranularity = granularity;
        this.table = new QTable();
    }
    
    /**
     * Returns an empty State1 instance. This means that no phase duration
     * should be set.
     * 
     * @return An empty state.
     */
    public abstract State1 getEmptyState();
    
    /**
     * Returns an empty Action1 instance. This means that no phase action should
     * be set.
     * 
     * @return An empty action.
     */
    public abstract Action1 getEmptyAction();
    
    /**
     * Copies the specified state to a new State1 instance.
     * 
     * @param state The state to be copied.
     * @return The copied state.
     */
    public abstract State1 getCopy(State1 state);
    
    /**
     * Copies the specified action to a new Action1 instance.
     * 
     * @param action The action to be copied.
     * @return The copied action.
     */
    public abstract Action1 getCopy(Action1 action);
    
    /**
     * Adds extended tuples (state, action) to the specified q-table.
     * 
     * @param table The q-table.
     * @param state The base state.
     * @param actions The base actions.
     */
    public abstract void addToTable(QTable table, State1 state,
            List<Action1> actions);
    
    /**
     * Returns the minimum duration of a phase.
     * 
     * @return The minimum duration of a phase.
     */
    public int getMinimumDuration() {
        return minimumDuration;
    }
    
    /**
     * Returns the maximum duration of a phase.
     * 
     * @return The maximum duration of a phase.
     */    
    public int getMaximumDuration() {
        return maximumDuration;
    }
    
    /**
     * Returns the granularity of the phases' duration.
     * 
     * @return The granularity of the phases' duration.
     */    
    public int getDurationGranularity() {
        return durationGranularity;
    }

    @Override
    public State getState() {
        return state;
    }    
    
    public void onSimulationStep(int currentSimStep) {        
    }
    
    @Override
    public QTable init() {
        state = getEmptyState();
        
        // Find indexes of phases that will be controlled.
        int index = 0;
        Deque<Integer> indexes = new ArrayDeque<Integer>();
        SumoTrafficLightProgram program = trafficLight.getProgram();        
        for (SumoTrafficLightProgram.Phase phase: program.getPhases()) {
            if (phase.getDuration() >= minimumDuration) {
                indexes.add(index);
                // Rounds down durations that aren't valid.
                state.setDuration(index, minimumDuration + durationGranularity *
                        ((phase.getDuration() - minimumDuration) /
                        durationGranularity));
            }
            index++;
        }
        
        LinkedList<Action1> actions = new LinkedList<Action1>();
        actions.add(getEmptyAction());
        initTable(getEmptyState(), actions, indexes, program);
        trafficLight.setProgram(state.toString());
        
        return table;
    }

    @Override
    public void execute(Action action) {
        Action1 action1 = (Action1) action;
        
        // Changes state locally.
        for (Integer index: action1.getIndexes()) {
            switch (action1.getDuractionAction(index)) {
            case DECREASE:
                state.setDuration(index, state.getDuration(index) - 
                        getDurationGranularity());
                break;
            case INCREASE:
                state.setDuration(index, state.getDuration(index) +
                        getDurationGranularity());
                break;
            default:
            }
        }
        
        // Change state in simulation.
        trafficLight.setProgram(state.toString());
    }
    
    /**
     * Initializes the q-table recursively with states of type State1 and
     * actions of type Action1.
     * 
     * @param state A partially built state.
     * @param actions The partially built actions for the partially built state.
     * @param indexes The phases' indexes that are not set in the partially
     * built state and action.
     */
    private void initTable(State1 state, List<Action1> actions,
            Deque<Integer> indexes, SumoTrafficLightProgram program) {
        // No more phases to set. Add (state, action) to table and the 
        // correspondent program to simulation.
        if (indexes.size() == 0) {
            addToTable(table, state, actions);

            int index = 0;
            SumoTrafficLightProgram newProgram =
                    new SumoTrafficLightProgram(state.toString());
            for (SumoTrafficLightProgram.Phase phase: program.getPhases()) {
                if (state.getIndexes().contains(index)) {
                    newProgram.addPhase(state.getDuration(index), 
                            phase.getState());                    
                } else {
                    newProgram.addPhase(phase.getDuration(), phase.getState());
                }
                index++;
            }
            trafficLight.setProgram(newProgram);
            return;
        }

        // Verifies current phase and creates the necessary tuples
        // (state, action) in which the phase duration will vary between the
        // specified values.
        Integer index = indexes.poll();
        for (int duration = minimumDuration; duration <= maximumDuration;
                duration += durationGranularity) {
            // Sets current phase state.
            State1 nextState = getCopy(state);
            nextState.setDuration(index, duration);

            List<Action1> nextActions = new LinkedList<Action1>();
            // Sets current phase action. Some duration actions are not valid.
            for (Action1 action: actions) {
                Action1 nextAction = getCopy(action);
                nextAction.setDurationAction(index, DurationAction.MAINTAIN);
                nextActions.add(nextAction);
                
                if (duration > minimumDuration) {
                    nextAction = getCopy(action);
                    nextAction.setDurationAction(index, DurationAction.DECREASE);                
                    nextActions.add(nextAction);
                }
                if (duration < maximumDuration) {
                    nextAction = getCopy(action);
                    nextAction.setDurationAction(index, DurationAction.INCREASE);
                    nextActions.add(nextAction);
                }             
            }
            
            // Continues to next phase.
            initTable(nextState, nextActions, indexes, program);
        }
        indexes.add(index);
    }

}
