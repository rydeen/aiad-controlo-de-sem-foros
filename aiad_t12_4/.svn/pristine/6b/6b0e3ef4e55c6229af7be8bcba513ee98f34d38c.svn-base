package main.agents;

import java.util.ArrayList;

import trasmapi.genAPI.TrafficLight;
import trasmapi.genAPI.exceptions.UnimplementedMethod;
import trasmapi.sumo.SumoTrafficLight;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;

/**
 * 
 * Traffic Light Agent class.
 * 
 * Copyright (C) AIAD @ FEUP project authors. All rights reserved.
 * 
 * @author <b>Filipe Oliveira</b>, <b>Paulo Araújo</b> and <b>Tiago Azevedo</b>
 * 
 */
public abstract class TLAgent extends Agent
{
	private static final long serialVersionUID = 6095960260125307076L;
	protected TrafficLight tl;
	protected ArrayList<String> controlledLanes;

	public TLAgent(String tlID) 
	{
		super();
		try
		{
			tl = new SumoTrafficLight(tlID);
			controlledLanes = tl.getControlledLanes();
			
		} catch (UnimplementedMethod e) 
		{
			e.printStackTrace();
		}
	}

	@Override
	protected void setup() 
	{
		DFAgentDescription ad = new DFAgentDescription();
		ad.setName(getAID());

		ServiceDescription sd = new ServiceDescription();
		sd.setName(getName());

		sd.setType("TrafficLightAgent");
		ad.addServices(sd); 

		try 
		{
			DFService.register(this, ad);
		} catch(FIPAException e) 
		{
			e.printStackTrace();
		}


		super.setup();
	}

	@Override
	protected void takeDown() 
	{
		try 
		{
			DFService.deregister(this);  
		} catch(FIPAException e) 
		{
			e.printStackTrace();
		}
		super.takeDown();
	}
}
