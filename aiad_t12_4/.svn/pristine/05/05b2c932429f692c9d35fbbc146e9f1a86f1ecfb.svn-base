package main.simulation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import main.agents.LearningTLAgent;
import main.agents.TLAgent;
import trasmapi.genAPI.*;
import trasmapi.genAPI.exceptions.*;
import trasmapi.sumo.Sumo;
import trasmapi.sumo.SumoTrafficLight;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;
import jade.wrapper.StaleProxyException;

/**
 * The main simulation of the application.
 */
public class MainSimulation {
        
    public static void main(String[] args) throws IOException, TimeoutException,
            UnimplementedMethod, InterruptedException, StaleProxyException {
        // Jade initialization.
        ProfileImpl profile = new ProfileImpl();
        ContainerController mainContainer =
                Runtime.instance().createMainContainer(profile);
        
        // Sumo initialization.
        Sumo sumo = new Sumo("guisim");
        List<String> params = new ArrayList<String>();
        params.add("-c=TlMap/map.sumo.cfg");
        
        try {
            SumoConfig conf = SumoConfig.load("TlMap/map.sumo.cfg");
            sumo.addParameters(params);
            sumo.addConnections("localhost", conf.getLocalPort());
        } catch (Exception e) {
            e.printStackTrace();
        }

        // TraSMAPI initialization.
        final TraSMAPI api = new TraSMAPI(); 
        api.addSimulator(sumo);
        api.launch();
        api.connect();
        api.start();        
        
        // Waits for SUMO to load. Is enough?
        Thread.sleep(1000);
        
        // Consults traffic lights and creates an agent for each one.
        ArrayList<String> ids = SumoTrafficLight.getIdList();
        final List<TLAgent> agents = new LinkedList<TLAgent>();
        List<AgentController> controllers = new LinkedList<AgentController>();
        
        System.out.print("Initiating agents ");
        for (int i = 0; i < ids.size(); i++) {
            TLAgent agent = new LearningTLAgent(ids.get(i));
            agents.add(agent);
            controllers.add(mainContainer.acceptNewAgent("TL#" + ids.get(i),
                    agent));
            System.out.print(".");
        }
        System.out.println("");
        
        // Adds drivers to the simulation.
        new DriversManagement().addDrivers();
        
        // Starts agents.
        for (AgentController controller: controllers) {
            controller.start();
        }
        
        // Updates simulation state.
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            
            @Override
            public void run() {
                try {
                    api.simulationStep(0);
                } catch (UnimplementedMethod e) {
                    e.printStackTrace();
                }
                for (TLAgent agent: agents) {
                    agent.onSimulationStep();
                }
            }
        }, 0, 100);
    }

}
