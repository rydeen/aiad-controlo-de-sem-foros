package main.qlearning;

/**
 * Implements a generic q-learning algorithm.
 */
public class QLearning {
    
    /** Current state. */
    private State state;
    
    /** Algorithm q-table. */
    private QTable table;
    
    /** Reward function. */
    private Reward reward;
    
    /** Action selection method. */
    private Selection selection;
    
    /** Action executor*/
    private Executor executor;
    
    /** Learning rate. */
    private float learningRate;
    
    /** Discount factor. */
    private float discountFactor;
    
    /**
     * Creates a QLearning instance.
     * 
     * @param state The initial state.
     * @param table The algorithm q-table.
     * @param lr The learning rate.
     * @param df The discount factor.
     */
    public QLearning(State state, QTable table, Reward reward,
            Selection selection, Executor executor, float lr, float df) {
        this.state = state;
        this.table = table;
        this.reward = reward;
        this.selection = selection;
        this.executor = executor;
        this.learningRate = lr;
        this.discountFactor = df;
    }
    
    /**
     * Applies a q-learning step.
     */
    public void step() {
        State old = state;
        
        Action action = selection.getSelection(state, table);
        state = executor.execute(action);
        
        float value =(1.0f - learningRate) * table.getValue(old, action) +
                learningRate * (reward.getValue(old, action) + discountFactor * 
                        table.getMaxValue());
        table.setValue(old, action, value);
    }
    
    /**
     * Sets the reward function.
     * 
     * @param reward The new reward function.
     */
    public void setReward(Reward reward) {
        this.reward = reward;
    }
    
    /**
     * Sets the action selection method.
     * 
     * @param selection The new action selection method.
     */
    public void setSelection(Selection selection) {
        this.selection = selection;
    }
    
    /**
     * Sets the learning rate.
     * 
     * @param learningRate The new learning rate.
     */
    public void setLearningRate(float learningRate) {
        this.learningRate = learningRate;
    }
    
    /**
     * Sets the discount factor.
     * 
     * @param discountFactor The new discount factor.
     */
    public void setDiscountFactor(float discountFactor) {
        this.discountFactor = discountFactor;
    }
    
}