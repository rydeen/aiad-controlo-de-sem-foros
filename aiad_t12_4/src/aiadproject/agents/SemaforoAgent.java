package aiadproject.agents;

import java.util.LinkedList;
import java.util.List;

import trasmapi.sumo.SumoTrafficLight;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;

/** Os objetos desta classe representam os agentes que controlam os sem�foros ! */

public abstract class SemaforoAgent extends Agent {
	
	@Override
	public String toString() {
		return "AgenteSem�foro: "+semaforo.getId();
	}

	private static final long serialVersionUID = 1L;

	// sem�foro do sumo controlado pelo agente
	protected SumoTrafficLight semaforo;
	protected List<SemaforoAgent> agentes_vizinhos;

	/**
	 * Contrutor da classe. Cria um Agente
	 * @param sem_sumo_id : id do sem�foro do sumo
	 */
	public SemaforoAgent( String sem_sumo_id ) {
		super();
		semaforo = new SumoTrafficLight( sem_sumo_id );
		agentes_vizinhos = new LinkedList<SemaforoAgent>();
	}

	/**
	 * A��o do agente a cada passo da simula��o
	 * @param tempo_simulacao 
	 */
	public void onSimulationStep(int tempo_simulacao) {}

	/**
	 * Devolve o sem�foro controlado no sumo
	 * @return SumoTrafficLight
	 */
	public SumoTrafficLight getSemaforo() { return semaforo; }

	/**
	 * Adiciona um agente � lista de agentes vizinhos
	 * @param agente
	 */
	public void addVizinho(SemaforoAgent agente){ agentes_vizinhos.add(agente); }
	
	/**
	 * Devolve o agente na posi��o 'i' na lista de vizinhos
	 * @param i
	 * @return SemaforoAgent
	 */
	public SemaforoAgent getVizinho(int i){ return agentes_vizinhos.get(i); }

	public List<SemaforoAgent> getVizinhos(){return agentes_vizinhos;}
	
	@Override
	protected void setup() {
		DFAgentDescription ad = new DFAgentDescription();
		ad.setName(getAID());

		ServiceDescription sd = new ServiceDescription();
		sd.setName(getName());

		sd.setType(semaforo.getId());
		ad.addServices(sd); 

		try {
			DFService.register(this, ad);
		} catch(FIPAException e) {
			e.printStackTrace();
		}

		super.setup();
	}

	@Override
	protected void takeDown() {
		try {
			DFService.deregister(this);  
		} catch(FIPAException e) {
			e.printStackTrace();
		}
		super.takeDown();
	}

	// fun��o para fazer override
	public void addComportamento() {}
	

	
	
}
