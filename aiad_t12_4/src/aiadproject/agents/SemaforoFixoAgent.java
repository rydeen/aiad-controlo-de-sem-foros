package aiadproject.agents;

import java.util.LinkedList;
import trasmapi.sumo.SumoTrafficLight;

/** Agente controlador de sem�foros: PLANO SEMAF�RIO FIXO */

public class SemaforoFixoAgent extends SemaforoAgent {


	private static final long serialVersionUID = 1L;
	

	public SemaforoFixoAgent( String sem_sumo_id ) {
		super( sem_sumo_id );
		semaforo = new SumoTrafficLight( sem_sumo_id );
		agentes_vizinhos = new LinkedList<SemaforoAgent>();
	}
	
	public void onSimulationStep(int tempo_simulacao) 
	{
		// os agentes sem�foro seguem os planos definidos por defeito
		// --> cada agente opera individualmente
		/*
			System.out.println("------------------------------------------------") ;
			System.out.println("ID:  " + semaforo.getId() ) ;
			System.out.println("STATE:  " + semaforo.getState() ) ;
			System.out.println("CURRENT PHASE DURATION:  " + semaforo.getCurrentPhaseDuration() ) ;
			System.out.println("PROGRAM:\n" + semaforo.getProgram().toString() ) ;
			System.out.println("PROGRAM DURATION:  " + semaforo.getProgram().getDuration() );
			System.out.println("CONTROLLED LANES:  " + semaforo.getControlledLanes() ) ;
			System.out.println("...foreach lane...") ;
			for( String lane : semaforo.getControlledLanes() ) {
				SumoLane sumo_lane = new SumoLane( lane ) ;
				System.out.println("[" + sumo_lane + "]" ) ;
				System.out.println("   num vehicles:  " + sumo_lane.getNumVehicles() ) ;
				System.out.println("   edge id:  " + sumo_lane.getEdgeId() ) ;
				System.out.println("   ...and for this edge..." ) ; 
				SumoEdge sumo_edge = new SumoEdge( sumo_lane.getEdgeId() ) ;
				System.out.println("      num vehicles:  " + sumo_edge.getNumVehicles() ) ;
			}
		
		
			System.out.println( "------------------------------------------------" ) ;
			System.out.println( "ID:  " + semaforo.getId() ) ;
			
			ArrayList<String> lanes = semaforo.getControlledLanes() ;
			for( int i=0 ; i<semaforo.getControlledLanes().size() ; i++ )
			{
				SumoLane sl = new SumoLane( lanes.get(i) ) ;
				System.out.println( sl.getId() + " | " + semaforo.getState().charAt(i) + " | " + sl.getNumVehicles() );
			}
		*/
	}
}
