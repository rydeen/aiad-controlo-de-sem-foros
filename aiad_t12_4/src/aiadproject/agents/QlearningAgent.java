package aiadproject.agents;

import jade.core.Agent;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Vector;

import aiadproject.simulation.MainSimulacao;
import aiadproject.ql_algorithm.QLearning;
import trasmapi.sumo.SumoLane;
import trasmapi.sumo.SumoTrafficLightProgram.Phase;

public class QlearningAgent extends SemaforoAgent {


	/**
	 * Classe do comportamento de um agente sem�foro QLearning.
	 * @author Luis Abreu
	 *
	 */
	private class QlearningBehavior extends SimpleBehaviour {

		private static final long serialVersionUID = 1L;
		
		// pilha com os valores de rewards recebidos a cada itera��o
		private Queue<Float> rewardQueue ;
		private int queue_max_size ;
		
		public QlearningBehavior(Agent a , int queue_size ) 
		{ 
			super(a) ; 
			this.rewardQueue = new LinkedList<Float>() ; 
			this.queue_max_size = queue_size ;
		}
		
		@Override
		public void action() 
		{
			ACLMessage msg = blockingReceive() ;
			if(msg.getPerformative() == ACLMessage.REQUEST)
			{
				//System.out.println("recebi um request!") ;
				ACLMessage reply = msg.createReply() ;
				reply.setPerformative(ACLMessage.INFORM);
				
				
				QlearningAgent aux_agent = (QlearningAgent) this.getAgent() ;
				String[] parts = msg.getContent().split("_") ;
				reply.setContent( aux_agent.getReward( parts[2] ) );
				send( reply ) ;
				
			}
			else if ( msg.getPerformative() == ACLMessage.INFORM )
			{
				//System.out.print( "recebi um inform! > " + msg.getContent() ) ;
				storeRewardValue( Float.parseFloat(msg.getContent()) ) ;
			} 
			
		}
	
		@Override
		public boolean done() {
			return false;
		}
	
		/**
		 * Adiciona valor de reward � queue de rewards recebidos
		 * @param reward
		 */
		private void storeRewardValue( float reward ) 
		{ 
			if( queue_isNotFull() )
				this.rewardQueue.add( reward ) ; 
		}
		
		private boolean queue_isNotFull() {
			return (this.rewardQueue.size() < this.queue_max_size) ;
		}
	}

	
	
	private static final long serialVersionUID = 1L;
	
	// Y para equa��o do Q-Learning
	static final float gamma = 0.69f;
	
	// Programa de um sem�foro - sequ�ncia de luzes
	private Vector<String> states;
	// Sequ�ncia atual em execu��o no sumo
	int currentIndex;
	
	// algoritmo qlearning
	public QLearning algorithm ;
	
	
	private QlearningBehavior agent_behavior ;
	
	/**
	 * Contrutor da classe.
	 * @param sem_sumo_id: id do sem�foro no sumo
	 */
 	public QlearningAgent(String sem_sumo_id) {
		super(sem_sumo_id);
		currentIndex = -1;
		this.algorithm = new QLearning( gamma , this ) ;
		this.states = new Vector<String>() ;
	}

 	/**
 	 * Devolve um inteiro no formato de String com o n�mero de carros na lane comum entre o sem�foro e o autor do pedido
 	 * @param agentId : id do sem�foro que fez o pedido REQUEST
 	 * @return String
 	 */
	public String getReward( String agentId ) 
	{
		int i_reward = 0 ;
		
		for( String s : this.semaforo.getControlledLanes() )
		{
			 if ( s.split("to")[0].equals( agentId ) )
			 {
				 SumoLane aux = new SumoLane(s) ;
				 i_reward = aux.getNumVehicles() ;
			 }
		}
		
		return Integer.toString( i_reward ) ;
	}

	/**
	 * Altera a fase das luzes de um cruzamento para a pr�xima no programa
	 */
	public void setNextPhase() {
		currentIndex++;
		currentIndex = (currentIndex % states.size());

		semaforo.setState( states.elementAt(currentIndex) );
	}
	
	/**
	 * Devolve o valor da pr�xima fase de luzes. Por exemplo "rrrGGgrrrGGg"
	 * @return String
	 */
	public String getNextPhase() {
		return this.states.elementAt( (currentIndex+1) % states.size() ) ;
	}

	/**
	 * Executa uma a��o
	 * @param action
	 */
	public void executeAction(boolean action) 
	{
		// TRUE  - Alterear a fase de um sem�foro para a seguinte
		// FALSE - Manter a fase de um sem�foro
		
		if(action)
			this.setNextPhase();
	}
	
	/**
	 * Devolve o programa semaforico do agente
	 * @return
	 */
	public Vector<String> getPossibleStates()
	{
		return this.states ;
	}
	
	/**
	 * Elimina todos os dados da queue de rewards
	 */
	public void clearRewardQueue() { this.agent_behavior.rewardQueue.clear(); }
	
	/**
	 * Retorna verdadeiro se a queue j� tiver todos os valores de rewards. Caso contr�rio retorna falso
	 * @return true ou false
	 */
	public boolean isRewardQueueFull() 
	{ 
		return ( this.agent_behavior.rewardQueue.size() == this.agentes_vizinhos.size() ) ;
	}
	
	/**
	 * Devolve a pilha das rewards
	 * @return Queue<Float>
	 */
	public Queue<Float> getRewardQueue() { 
		return this.agent_behavior.rewardQueue ; 
	}
	
	/**
	 * Envia um REQUEST de rewards aos agentes sem�foro vizinhos
	 */
    public void sendMessage() 
    {	
    	//System.out.println( semaforo.getId() + ": Enviei um pedido!") ;
		ACLMessage msg  = new ACLMessage(ACLMessage.REQUEST);
		msg.setContent("reward_to_" + semaforo.getId() );
		
		for( SemaforoAgent sa : this.agentes_vizinhos )
		{
			msg.addReceiver( sa.getAID() ) ;
		}
		
		send(msg) ;
    }
    
    @Override
    public void addComportamento() {
    	agent_behavior = new QlearningBehavior(this, this.getVizinhos().size() );
		addBehaviour( agent_behavior ) ;
    }
    
    public void onSimulationStep( int time ) 
    {
    	//System.out.println("TIME: " + time );
    	if ( time == 200 )
    	{		
    		List<Phase> lista = semaforo.getProgram().getPhases();
    		
    		for( Phase s: lista ) 
    			states.addElement( s.getState() );
    		
    		//System.out.println("----------------------------------------------");
    		//System.out.println( semaforo.getId() + ": " + states.toString() ) ;
    		this.setNextPhase() ;
    		this.algorithm.initTable();
    	}
    	else if ( (time % 1000 == 0) )
    	{
    		if( time < MainSimulacao.QLearningStart ) // tempo inferior a 12 minutos >>> 1a fase da simula��o
    		{
    			// na primeira fase, controi-se a tabela Q depois de usar 
    			this.sendMessage() ;
    			algorithm.executaIteracao();
    		}
    		else // a partir dos 15 minutos, come�a a segunda fase de simula��o
    		{
    			algorithm.executeIterQ(time) ;
    		}
    	}
    }
}
