package aiadproject.simulation;

import java.util.LinkedList;
import java.util.Random;

import aiadproject.simulation.MainSimulacao.ModoAgentesSemaforo;
import trasmapi.sumo.SumoCom;
import trasmapi.sumo.SumoVehicle;

 
/**
 * Gest�o de ve�culos do SUMO.
 */
public class GestaoVeiculos {
    
    private int numeroCarros = 0;
    
    private Random rand = new Random(25678134);
    
    // numero de carros a simular por cada 'inc_tempo_simulacao' milisegundos
    private final int carrosPorEstado__MIN = 2 ;
    private final int carrosPorEstado__MED = 5 ;
    private final int carrosPorEstado__MAX = 10 ;
    
    // 15 000 milisegundos = 15 segundos --- ideia: spawn de veiculos de 15 em 15 segundos
    private final int inc_tempo_simulacao = 15000;
    // 600 000 milisegundos = 600 segundos = 10 minutos !
    private final int tempo_simulacao_1 = 600000;
    
    // tempos para a segunda parte da simula��o :: utilizado apenas na simula��o com q-learning
    private final int tempo_simulacao_2_init = 900000; // 15 minutos
    private final int tempo_simulacao_2_end = 1800000; // 30 minutos
    
    // -- 0/3to1/3 significa que � a rua entre os cruzamentos 0/3 e 1/3
    // -- 0/3 significa que � o cruzamento na posi��o (0,3) da matriz
    
    private String[] pontos_origem = {"0/3to1/3", "1/3to2/3", "2/3to3/3", "3/3to3/2", "3/2to3/1", "3/1to3/0",
    		                       	  "3/0to2/0", "2/0to1/0", "1/0to0/0", "0/0to0/1", "0/1to0/2", "0/2to1/2",
    		                          "1/2to2/2", "2/2to2/1", "2/1to1/1"};
    
    private String[] pontos_destino = {"1/0to0/0", "0/0to0/1", "0/1to0/2", "0/2to1/2","1/2to2/2", "2/2to2/1", 
    		                           "0/3to1/3", "1/3to2/3", "2/3to3/3", "3/3to3/2", "3/2to3/1", "3/1to3/0",
                                       "3/0to2/0", "2/0to1/0", "2/1to1/1"};
    
    private String[] mediumOL = {"0/2to1/2", "0/1to1/1", "3/2to2/2", "3/1to2/1"};
    private String[] mediumDL = {"3/2to3/1", "3/1to3/2", "0/1to0/0", "0/2to0/3"};

    
    public LinkedList<SumoVehicle> geradorCarros() 
    {
        SumoCom.createAllRoutes();
        
        LinkedList<SumoVehicle> lista_veiculos = new LinkedList<SumoVehicle>();

        int veiculo_id = 0;
        int index_origem = 0;
        int index_destino = 0;
        
    	for ( int tempo_simulacao_atual = 0 ; 
    			tempo_simulacao_atual < tempo_simulacao_1 ; tempo_simulacao_atual += inc_tempo_simulacao )
    	{
    		numeroCarros = getNumeroCarros(tempo_simulacao_atual);
    		
    		for ( int i = 0 ; i < numeroCarros ; i++ ) 
    		{
    			if ( numeroCarros != carrosPorEstado__MED )
    			{
    				index_origem = rand.nextInt( pontos_origem.length );
    				index_destino = rand.nextInt( pontos_destino.length );
    			}
    			else
    			{
    				index_origem = index_destino = rand.nextInt( mediumOL.length );
    			}
    			
    			String origem = getOrigem(index_origem, numeroCarros);
    			String destino = getDestino(index_destino, numeroCarros);
    			
    			String tipoVeiculo = SumoCom.vehicleTypesIDs.get( rand.nextInt(SumoCom.vehicleTypesIDs.size()) );
    			String route_id = SumoCom.getRouteId(origem, null);
    			
    			
    			if ( origem.equals(destino) )
    			{
    				int novo_index = 0;
    				if ( index_destino == 0 ) 
    					novo_index = 3;
    				
    				destino = getDestino(novo_index, numeroCarros);
    			}

    			SumoVehicle veiculo = new SumoVehicle(
    					veiculo_id++ , tipoVeiculo , route_id , tempo_simulacao_atual , 0 , 0 , (byte) 0 ) ;
    			
    			SumoCom.addVehicle( veiculo ) ;
        		SumoCom.addVehicleToSimulation( veiculo );
        		
    			veiculo.changeTarget(destino);    			
    			lista_veiculos.add(veiculo);
    		}
    	}
    	
    	//---------------------------------------------------------------------------//
    	// S� PARA AGENTES QLEARNING ------ CARROS PARA A SEGUNDA PARTE DA SIMULA��O //
    	//---------------------------------------------------------------------------//
    	
    	if( MainSimulacao.modo_agentes_semaforo == ModoAgentesSemaforo.Q_LEARNING )
    	{
    		for ( int tempo_simulacao_atual = tempo_simulacao_2_init ; 
        			tempo_simulacao_atual < tempo_simulacao_2_end ; tempo_simulacao_atual += inc_tempo_simulacao )
        	{
        		numeroCarros = getNumeroCarros(tempo_simulacao_atual);
        		
        		for ( int i = 0 ; i < numeroCarros ; i++ ) 
        		{
        			if ( numeroCarros != carrosPorEstado__MED )
        			{
        				index_origem = rand.nextInt( pontos_origem.length );
        				index_destino = rand.nextInt( pontos_destino.length );
        			}
        			else
        				index_origem = index_destino = rand.nextInt( mediumOL.length );
        			
        			String origem = getOrigem(index_origem, numeroCarros);
        			String destino = getDestino(index_destino, numeroCarros);
        			
        			String tipoVeiculo = SumoCom.vehicleTypesIDs.get( rand.nextInt(SumoCom.vehicleTypesIDs.size()) );
        			String route_id = SumoCom.getRouteId(origem, null);
        			
        			
        			if ( origem.equals(destino) )
        			{
        				int novo_index = 0;
        				if ( index_destino == 0 ) 
        					novo_index = 3;
        				
        				destino = getDestino(novo_index, numeroCarros);
        			}

        			SumoVehicle veiculo = new SumoVehicle(
        					veiculo_id++ , tipoVeiculo , route_id , tempo_simulacao_atual , 0 , 0 , (byte) 0 ) ;
        			
        			SumoCom.addVehicle( veiculo ) ;
            		SumoCom.addVehicleToSimulation( veiculo );
            		
        			veiculo.changeTarget(destino);    			
        			lista_veiculos.add(veiculo);
        		}
        	}
    	}
    	//---------------------------------------------------------------------------//
    	//---------------------------------------------------------------------------//
    	
    	
        return lista_veiculos;
    }

    
    private int getNumeroCarros( int tempo_simulacao )
    {
    	/* Gerar carrosPorEstado__MIN veiculo a cada 15 segundos at� aos 2 minutos de simula��o
    	 * Gerar carrosPorEstado__MAX veiculos a cada 15 segundos at� aos 4 minutos de simula��o
    	 * Gerar carrosPorEstado__MED veiculos a cada 15 segundos at� aos 6 minutos e meio de simula��o
    	 * Gerar carrosPorEstado__MIN veiculo a cada 15 segundos at� ao final da simula��o (10 minutos)
    	 * */
    	
    	/* No caso da simulacao com qlearning a segunda parte come�a aos 15 minutos
    	 * Gerar carrosPorEstado__MIN veiculo a cada 15 segundos at� aos 17 minutos de simula��o
    	 * Gerar carrosPorEstado__MAX veiculos a cada 15 segundos at� aos 19 minutos de simula��o
    	 * Gerar carrosPorEstado__MED veiculos a cada 15 segundos at� aos 21 minutos e meio de simula��o
    	 * Gerar carrosPorEstado__MIN veiculo a cada 15 segundos at� ao final da simula��o (25 minutos)
    	 * */
    	
    	int spawn_lento_1 = 120000 ; // 2 minutos
    	int spawn_agressivo_1 = 240000 ; // 4 minutos
    	int spawn_medio_1 = 360000 ; // 6,5 minutos
    	int final_1 = 600000 ; // 10 minutos
    	
    	int inicio_2 = 900000 ; // 15 minutos
    	int spawn_lento_2 = 1020000 ; // 17 minutos
    	int spawn_agressivo_2 = 1140000 ; // 19 minutos
    	int spawn_medio_2 = 1290000 ; // 21,5 minutos
    	int final_2 = 1500000 ; // 25 minutos
    	
    	if ( tempo_simulacao < spawn_lento_1 )
    		return carrosPorEstado__MIN ;
    	else if ( tempo_simulacao < spawn_agressivo_1 ) 
    		return carrosPorEstado__MAX ;
    	else if ( tempo_simulacao < spawn_medio_1 ) 
    		return carrosPorEstado__MED ;
    	else if ( tempo_simulacao < final_1 )
    		return carrosPorEstado__MIN;
    	else if ( tempo_simulacao >= inicio_2 )
    	{
    		if ( tempo_simulacao < spawn_lento_2 )
        		return carrosPorEstado__MIN ;
    		else if ( tempo_simulacao < spawn_agressivo_2 ) 
        		return carrosPorEstado__MAX ;
    		else if ( tempo_simulacao < spawn_medio_2 ) 
        		return carrosPorEstado__MED ;
    		else if ( tempo_simulacao < final_2 )
        		return carrosPorEstado__MIN;
    	}
    	
    	return 0 ;
    }
    
    private String getOrigem(int index, int numDrivers){
    	if(numDrivers == carrosPorEstado__MED){
    		return mediumOL[index];
    	}
    	else{
    		return pontos_origem[index];
    	}
    }
    
    private String getDestino(int index, int numDrivers){
    	if(numDrivers == carrosPorEstado__MED){
    		return mediumDL[index];
    	}
    	else{
    		return pontos_destino[index];
    	}
    }

}
