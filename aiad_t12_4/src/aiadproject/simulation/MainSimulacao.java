package aiadproject.simulation;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import aiadproject.Utils.Statistics;
import aiadproject.agents.QlearningAgent;
import aiadproject.agents.SemaforoAgent;
import aiadproject.agents.SemaforoFixoAgent;
import trasmapi.genAPI.*;
import trasmapi.genAPI.exceptions.*;
import trasmapi.sumo.Sumo;
import trasmapi.sumo.SumoCom;
import trasmapi.sumo.SumoTrafficLight;
import trasmapi.sumo.SumoVehicle;
import jade.BootProfileImpl;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;
import jade.wrapper.StaleProxyException;

/**
 * Main do Simulador da aplica��o
 */
public class MainSimulacao {

	public enum ModoAgentesSemaforo { PLANO_FIXO , Q_LEARNING } ;
	
	// private static ModoAgentesSemaforo modo_agentes_semaforo = ModoAgentesSemaforo.PLANO_FIXO ;
	public static ModoAgentesSemaforo modo_agentes_semaforo = ModoAgentesSemaforo.Q_LEARNING ;
	
	// tempo m�ximo de simula��o em milisegundos: 1800000ms = 30 minutos
	public static final int SimulationTime = 1800000 ;
	public static final int QLearningStart = 600000 ;

	public static void main(String[] args) throws IOException, TimeoutException, UnimplementedMethod, InterruptedException, StaleProxyException 
	{
		/* ------------------------ inicializa��o da plataforma JADE ------------------------ */
		List<String> param_jade = new ArrayList<String>();
		// para executar a janela -- n�o � necess�rio no produto final
		// param_jade.add("-gui");
		ProfileImpl profile = new BootProfileImpl(param_jade.toArray(new String[0]));


		ContainerController mainContainer = Runtime.instance().createMainContainer(profile);
		/* ------------------------ inicializa��o do simulador SUMO ------------------------*/
		// executar em janela gr�fica
		Sumo sumo = new Sumo("guisim");
		List<String> param_sumo = new ArrayList<String>();

		// ficheiro de localiza��o das configura��es do simulador
		String sumo_conf_dir ="SumoConf/" ;
		String sumo_conf_filename = "map.sumo.cfg" ;
		String sumo_conf_flag = "-c=" ;

		param_sumo.add( sumo_conf_flag + sumo_conf_dir + sumo_conf_filename );

		try 
		{
			// vai ao ficheiro de configura��es e obtem a porta local para a execu��o
			SumoConfiguration conf = SumoConfiguration.load( sumo_conf_dir + sumo_conf_filename );            
			sumo.addParameters(param_sumo);
			sumo.addConnections("localhost", conf.getLocalPort());
		} 
		catch (Exception e) { e.printStackTrace(); }

		/* ------------------------ inicializa��o da api trasmapi ------------------------ */

		final TraSMAPI api = new TraSMAPI();

		// adicionar o simulador � api criada
		api.addSimulator(sumo);

		// iniciar a api
		api.launch(); 			// -- abre a janela do sumo
		api.connect();			// -- establece a liga��o : aqui o sumo j� tem acesso aos mapas e configura��es
		api.start();        

		// o sumo demora a carregar, � preciso esperar um pouco para que tudo esteja operacional
		Thread.sleep(1500);


		/* ------------------------ cria��o dos agentes sem�foro!  ------------------------ */

		// ir buscar todos os sem�foros criados no sumo
		ArrayList<String> ids = SumoTrafficLight.getIdList();

		// lista de agentes de sem�foros e controladores
		final List<SemaforoAgent> agentes_semaforo = new LinkedList<SemaforoAgent>();
		List<AgentController> controladores = new LinkedList<AgentController>();

		// inicializa��o dos agentes : criar tantos agentes quantos sem�foros existirem no sumo
		for (int i = 0; i < ids.size(); i++) 
		{
			SemaforoAgent novo_agente_semaforo ;
			if ( modo_agentes_semaforo.equals(ModoAgentesSemaforo.PLANO_FIXO) )
				novo_agente_semaforo = new SemaforoFixoAgent(ids.get(i));
			else // if ( modo_agentes_semaforo.equals(ModoAgentesSemaforo.Q_LEARNING) )
				novo_agente_semaforo = new QlearningAgent(ids.get(i));

			// adicionar o novo agente � lista
			agentes_semaforo.add( novo_agente_semaforo );

			// adicionar agente ao controlador principal da aplica��o: (nome do agente , agente)
			controladores.add( mainContainer.acceptNewAgent("AgenteSemaforo-#" + ids.get(i), novo_agente_semaforo));
		}
		
		/* ------------------------ calcula os vizinhos ------------------------ */
		// descobre os vizinhos de todos os sem�foros
		if ( modo_agentes_semaforo == ModoAgentesSemaforo.Q_LEARNING )
		{
			boolean found;
			for(int i = 0; i < agentes_semaforo.size(); i++)
			{
				for(int j = 0; j < agentes_semaforo.size(); j++)
				{
					found = false;
					if(i!=j){
	
						ArrayList<String> list1 = agentes_semaforo.get(i).getSemaforo().getControlledLanes();
						ArrayList<String> list2 = agentes_semaforo.get(j).getSemaforo().getControlledLanes();
	
						for(int z=0; z<list1.size(); z++)
						{
							for(int y = 0; y<list2.size(); y++)
							{
								if(sameLane(list1.get(z),list2.get(y))){
									found = true;
									
									//System.out.println("Adicionado vizinho");
									agentes_semaforo.get(i).addVizinho(agentes_semaforo.get(j));
									break;
								}	
							}
							if(found)
								break;
						}
	
					}else
						continue;
				}
			}
		}
		
		/* ------------------------ adiciona comportamentos aos sem�foros ------------------------ */
		if ( modo_agentes_semaforo == ModoAgentesSemaforo.Q_LEARNING )
		{
			for( SemaforoAgent agent : agentes_semaforo )
				agent.addComportamento();
		}
		
		
		/* ------------------------ adicionar os ve�culos do sumo � simula��o   ------------------------ */
		// cria um gestor : � o controlador que cria os veiculos e que os adiciona � simula��o
		GestaoVeiculos gestor_veiculos = new GestaoVeiculos();
		// cria e adiciona os veiculos ao simulador
		List<SumoVehicle> carros = gestor_veiculos.geradorCarros();
		Statistics estatistica = new Statistics(QLearningStart);
		// fazer subscribe dos veiculos
		for (SumoVehicle carro : carros)
		{
			SumoCom.subscribeVehicle( carro.id );
			estatistica.addStatisticalVehicle(carro);
		}

		// fazer start aos controladores dos agentes criados
		for (AgentController controller: controladores) {
			controller.start();
		}

		/* ------------------------ iniciar a simula��o ------------------------ */
		int estadoAtual = SumoCom.getCurrentSimStep();

		while( estadoAtual <= SimulationTime )
		{
			estadoAtual = SumoCom.getCurrentSimStep();
			
			if ( !api.simulationStep(0) )
			{
				break;
			}
			if( (estadoAtual % 4000 == 0) || estadoAtual==200 )
			for ( SemaforoAgent agente_semaforo: agentes_semaforo )
			{
				agente_semaforo.onSimulationStep( estadoAtual );
			}
			if(estadoAtual >= QLearningStart) //Gera estatistica apenas durante aplica��o de algoritmo Q-learning com aprendizagem realizada
				estatistica.update(estadoAtual);
		}
		
		estatistica.saveStatistics("./statistics/" + new SimpleDateFormat("ddhhmm").format(new Date()) + ".xls");
		api.close();
		System.exit(0);
		return;
		
		/* ------------------------ imprime tabelas Q ------------------------ */
		/*
		if ( modo_agentes_semaforo == ModoAgentesSemaforo.Q_LEARNING )
		{
			for( SemaforoAgent agent : agentes_semaforo )
			{ 
				System.out.println("***********************************************************");
				System.out.println("Semaforo ID: " + agent.getSemaforo().getId() ) ;
				System.out.println("Tabela Q:\n" + ((QlearningAgent) agent).algorithm.tableQ() ) ;
				System.out.println("-----------------------------------------------------------");
			}
		}
		*/
	}

	

	public static boolean sameLane(String l1, String l2)
	{
		String aux = invert(l1);
		if(cut(aux).equals(cut(l2)))
			return true;
		else
			return false;
	}
	
	public static String invert(String l1)
	{
		StringBuilder sb = new StringBuilder(l1.length());
		sb.append(l1.split("to")[1]);
		sb.append("to");
		sb.append(l1.split("to")[0]);
		return sb.toString();
	}
	
	public static String cut(String s)
	{
		return s.replaceAll("_0","");
	}
}
