package aiadproject.simulation;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Ficheiro de configuração do Sumo
 */
public class SumoConfiguration {
    
    /** Path do ficheiro de configuração. */
    private String filepath;
    
    /** Porta local de acesso */
    private int localPort;
    
    /** Marca a existir no ficheiro XML com as configurações do servidor traci */
    static final private String xml_traci_server_tag = "traci_server" ;
    
    /** Nó filho do 'traci_server' onde se encontra o valor da porta local do servidor */
    static final private String xml_remote_port_tag = "remote-port" ;
    
    /**
     * Cria uma instancio do objeto SumoConfig
     * @param filepath - path do ficheiro de configuração
     */
    private SumoConfiguration( String filepath ) {
        this.filepath = filepath;
    }
    
    /**
     * Devolve o path do ficheiro de configurações
     * @return o path do ficheiro
     */
    public String getFilePath() {
        return filepath;
    }

    /**
     * Devolve a porta local onde o sumo está a executar
     * @return A porta local
     */
    public int getLocalPort() {
        return localPort;
    }
    
    /**
     * Modifica o nome do path para o ficheiro de configurações
     * @param file The new file path.
     */
    public void setFile( String filepath ) {
        this.filepath = filepath;
    }

    /**
     * Altera o valor da porta local.
     * @param localPort - Novo valor para a porta local
     */
    public void setLocalPort( int localPort ) {
        this.localPort = localPort;
    }

    /**
     * Carregamento e retorno do ficheiro de configurações do sumo.
     * @param filepath - path para o ficheiro de configurações
     * @return objeto de configurações do sumo
     * @throws Exception
     */
    public static SumoConfiguration load( String filepath ) throws Exception 
    { 
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        
        File confFile = new File( filepath );
        
        // o DocumentBuilder faz parse de ficheiros xml para objetos Document
        Document document = builder.parse(confFile);
        SumoConfiguration conf = new SumoConfiguration( filepath );
        
        // um Element representa um elemento HTML ou XML, nós queremos ir ao ficheiro
        // de configurações e obter o número da porta local
        Element root = document.getDocumentElement();
        root.normalize();
        
        NodeList servidor = root.getElementsByTagName( xml_traci_server_tag );        
        NodeList nosServidor = servidor.item(0).getChildNodes();
        
        
        for (int i = 0; i < nosServidor.getLength(); i++) 
        {
            Node node = nosServidor.item(i);
            
            if ( node.getNodeType() == Node.ELEMENT_NODE && node.getNodeName().equals( xml_remote_port_tag ) ) 
            {
                conf.setLocalPort(Integer.valueOf( ( (Element)node ).getAttribute( "value" )) );
                break;
            }
        }
        
        return conf;
    }

}
