package aiadproject.ql_algorithm;

import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.Map.Entry;
import java.util.Random;

import aiadproject.Utils.CarQuantity;
import aiadproject.agents.QlearningAgent;
import aiadproject.simulation.MainSimulacao;

public class QLearning {

	// Tabela dos valores Q
    private Map<State, Map<Boolean, Float>> tabelaQ ;
    // Gamma da equa��o
	private float y ;
	// Agente sem�foro
	private QlearningAgent agente_semaforo ;
	
	// Acoes possiveis
	private final boolean A1_CHANGE = true ;
	private final boolean A2_KEEP = false ;
	// Valor inicial de todos os Q's
	private final float DEFAULT_INIT_Q = 0.0f ;
	
	
	private boolean continue_running = false ;
	private int stack_n_stops = 0 ;
	private final int stack_n_stops_max = 2 ;
	
	private float min_p = 0.0f; 
	private float max_p = 1.0f; 
	private float min_p_limit = 0.1f ; // probabilidade m�nima
	private float max_p_limit = 1.0f ; // probabilidade m�xima
	
	private float a ;
	private float b ;
	
	/**
	 * Contrutor de um objeto Q-Learning
	 * @param y valor gamma da equa��o
	 * @param agent agente sem�foro controlador
	 */
	public QLearning( float y , QlearningAgent agent ) 
	{
		this.y = y ;
		this.agente_semaforo = agent ;
		this.tabelaQ = new HashMap<State, Map<Boolean,Float>>();
		this.a = 0f;
		this.b = 0f;
		
		generateFuncaoExp( MainSimulacao.QLearningStart, max_p_limit , MainSimulacao.SimulationTime , min_p_limit );
	}
	
	/**
	 * Calcula a reward de uma a��o executada num estado
	 * @param estado
	 * @param acao
	 * @return o valor da recompensa da a��o
	 * @throws InterruptedException 
	 */
	private float calculaReward( State estado , boolean acao ) 
	{	
		//System.out.println(agente_semaforo.getSemaforo().getId() + 
		//		": queue_size = " + agente_semaforo.getRewardQueue().size() );
		
		Queue<Float> aux_queue = agente_semaforo.getRewardQueue() ;
		float max = (float) aux_queue.size() ;
		float reward = 0.0f ;
		
		while( ! aux_queue.isEmpty() ) {
			reward += (aux_queue.poll() / max) ;
		} 
		
		agente_semaforo.clearRewardQueue() ;

		return reward ;
	}
	
	/**
	 * Calcula o valor m�ximo de todas as a��es poss�veis de um 'estado'
	 * @param estado
	 * @return valor da a��o
	 */
	private float calculaMaxQ( State estado ) 
	{	
        //System.out.println("------------------------ TABELA Q ------------------------") ;
		//System.out.println( tableQ() ) ;
        //System.out.println("----------------------------------------------------------") ;
        //System.out.println("estado: " + estado ) ;
        //System.out.println("contem? " + tabelaQ.containsKey( estado ) ) ;
        //System.out.println("----------------------------------------------------------") ;
        //System.out.println("keyset: " + tabelaQ.keySet() );
        //System.out.println("----------------------------------------------------------") ;
        
		Map<Boolean,Float> acoes = tabelaQ.get( estado ) ;

		Map.Entry<Boolean, Float> max_acao = null ;

		for ( Map.Entry<Boolean, Float> entry : acoes.entrySet() )
		    if ( max_acao == null || entry.getValue().compareTo(max_acao.getValue()) > 0)
		        max_acao = entry;
		
		return max_acao.getValue() ;
	}
	
	/**
	 * Calcula o valor Q(s,a) atual
	 * @param estado_atual
	 * @param acao
	 * @return Q(s,a)
	 */
	public float calculaValorQ( State estado_atual , boolean acao ) 
	{	
		/* FORMULA */
		// Q(s,a) = R(s,a) + y*Max( Q(s,a') )
		
		float reward = this.calculaReward( estado_atual, acao ) ;
		//System.out.println("Reward: " + reward );
		
		float maxQ = this.calculaMaxQ( estado_atual ) ;
		//System.out.println("maxQ: " + maxQ );
		
		return (reward + (this.y*maxQ)) ;
	}
	
	/**
	 * inicializar a tabela dos valor Q
	 */
	public void initTable() 
	{
		Map<Boolean, Float> acao_q = new HashMap<Boolean, Float>() ;
		acao_q.put( A1_CHANGE , DEFAULT_INIT_Q ) ; 
		acao_q.put( A2_KEEP , DEFAULT_INIT_Q ) ;
		
		for ( String s : agente_semaforo.getPossibleStates() )
		{
			State st_0 = new State( s , CarQuantity.NONE ) ;
			State st_1 = new State( s , CarQuantity.FEW ) ;
			State st_2 = new State( s , CarQuantity.SOME ) ;
			State st_3 = new State( s , CarQuantity.MANY ) ;
			tabelaQ.put( st_0 , acao_q ) ;
			tabelaQ.put( st_1 , acao_q ) ;
			tabelaQ.put( st_2 , acao_q ) ;
			tabelaQ.put( st_3 , acao_q ) ;
		}
		
		continue_running = true ;
		
		//System.out.println("----------------------------------------------");
		//System.out.println( tableQ() ) ;
	}
	
	/**
	 * Calcula os valores para a fun��o de probabilidades (simulated annealing)
	 * @param tempoInicial
	 * @param probInicial
	 * @param tempoFinal
	 * @param probFinal
	 */
	public void generateFuncaoExp(float tempoInicial, float probInicial, float tempoFinal, float probFinal)
	{
		this.b = (float) Math.pow(probFinal,(1.0f/(tempoFinal-tempoInicial)));
		this.a = (float) (probInicial/Math.pow(b,tempoInicial));
	}
	
	/**
	 * Calcula a probabilidade de escolha da melhor a��o para um estado na tabela Q
	 * @param x: tempo de simula��o
	 * @return float
	 */
	public float calcProbAcaoRandom( int x ) 
	{
		// formula:  p(x) = 3.16227942504*0.99999808118^x
		
		float p = (float) (a * Math.pow( b , x )) ;
		return (float) (Math.round(p*100.0)/100.0) ;
	}
	
	/**
	 * Seleciona uma a��o aleatoriamente a ser executada
	 * @return a��o a tomar
	 */
	public boolean chooseRandomAction() 
	{
		return  new Random().nextBoolean() ;
	}
	
	/**
	 * Perante um State estado, escolhe a acao a tomar de acordo com os valores de Q na tabela
	 * @param estado
	 * @param prob_escolha
	 * @return boolean
	 */
	public boolean chooseAction( State estado ) 
	{
		
		
		Map<Boolean, Float> acoes_aux = this.tabelaQ.get( estado ) ;
		Map.Entry<Boolean, Float> acao = null ;
		
		// seleciona a a��o com o valor de Q mais baixo (neste caso, quanto maior Q, pior � para os vizinhos)
		for ( Map.Entry<Boolean, Float> entry : acoes_aux.entrySet() )
			if ( acao == null || entry.getValue().compareTo(acao.getValue())<0 )
				acao = entry ;
		
		//System.out.println("-------------- SELECT ACTION" + 
		//		this.agente_semaforo.getSemaforo().getId() + " -----------------") ;
		//System.out.println("estado atual: " + estado ) ;
		//System.out.println("objeto acoes: " + acoes_aux.toString() ) ;
		//System.out.println("objeto acao:  " + acao.getKey() ) ;
		
		
		// caso a a��o escolhida seja n�o alterar as cores do cruzamento,
		// temos de garantir que j� n�o � mil�sima vez e que o sem�foro est� bloqueado
		if( ! acao.getKey() )
		{
			if( this.stack_n_stops < this.stack_n_stops_max )
			{
				CarQuantity c ;
				c = CarQuantity.getValue( this.agente_semaforo.getSemaforo().getNumCarrosParados() ) ;
				
				if ( c == CarQuantity.MANY ) // se tenta manter as cores mas existem muitos carros parados
				{							 // no cruzamento, ent�o for�a a mudan�a para o pr�ximo estado
					this.stack_n_stops = 0 ;
					return true ;
				}
				
				this.stack_n_stops++ ;
				return acao.getKey() ;
			}
			else // se j� � a 3a vez consecutiva que tenta manter as luzes, for�a a mudan�a para o pr�ximo estado!
			{
				this.stack_n_stops = 0 ;
				return true ;
			}
		}
		
		return acao.getKey() ;
	}
	 
	/**
	 * Executa uma itera��o do algoritmo Q-Learning. As a��es s�o aleat�rias. Atualiza a tabela Q a cada itera��o
	 */
	public void executaIteracao() 
	{
		if( continue_running )
		{
	        State estado = new State( this.agente_semaforo ) ;
	        boolean acao = this.chooseRandomAction() ;
	        this.agente_semaforo.executeAction( acao ) ;
	        
	        //System.out.println("------------------------------------------------------");
	        //System.out.println("--------------------------" + this.agente_semaforo.getSemaforo().getId() + "-------------------------");
	        //System.out.println("------------------------------------------------------");
	        //System.out.println("Estado: " + estado.toString() );
	        //System.out.println("Acao: " + ( acao? "mudar" : "manter" ) + " {"+acao+"}");
	        
	        float q = this.calculaValorQ( estado , acao ) ;
	        
	        //System.out.println("Q: " + q );
	        //System.out.println("");
	        
	        this.actualizaTabelaQ(estado, acao, q) ;
		}
		//else
		//	System.out.println("Wait for it...") ;
	}
	
	/**
	 * executa uma itera��o do algoritmo Q, escolhendo a a��o de acordo com os valores da tabela Q.
	 */
	public void executeIterQ( int time ) 
	{
		if( continue_running )
		{			
			// calculo de um valor aleat�rio: segundo a probabilidade, escolher a melhor ou n�o???
			Random rand = new Random();
			float random_p = rand.nextFloat() * (max_p - min_p) + min_p;
			float p_acao_aleatoria = this.calcProbAcaoRandom( time ) ;
			
			boolean acao ;
			State estado = new State( this.agente_semaforo ) ;
			
			if ( random_p < p_acao_aleatoria )
			{
				//System.out.println("----------------------------------") ;
				//System.out.println("Random! (p_rand<p_calc): " + random_p + "<" +  p_acao_aleatoria);
				acao = this.chooseRandomAction() ;
			}
			else
			{
				//System.out.println("----------------------------------") ;
				//System.out.println("Best! (p_rand<p_calc): " + random_p + "<" +  p_acao_aleatoria);
				acao = this.chooseAction(estado) ;
			}
			
			this.agente_semaforo.executeAction( acao ) ;
			
			// continua aprendizagem....
			float q = this.calculaValorQ( estado , acao ) ;
			this.actualizaTabelaQ(estado, acao, q) ;
			
			//System.out.println( "-----------------------------------------" ) ;
			//System.out.println( "TL_ID: " + this.agente_semaforo.getSemaforo().getId() ) ;
			//System.out.println( "acao tomada: " + acao ) ;
		}
		//else
		//	System.out.println("Wait for it....");
	}
	
	/**
	 * Atualiza a tabela Q com um novo valor 'q' para um estado e uma acao
	 * @param estado
	 * @param acao
	 * @param q
	 */
	private void actualizaTabelaQ( State estado , boolean acao , float q ) 
	{
		Map<Boolean, Float> aux = this.tabelaQ.get( estado ) ;
		Map<Boolean, Float> m = new HashMap<Boolean, Float>() ;
		
		m.put( acao , q ) ;
		m.put( !acao , aux.get(!acao) ) ;
		
		this.tabelaQ.put( estado , m ) ;
	}

	/**
	 * Vers�o do toString() s� para a tabela Q
	 * @return tabela Q em string
	 */
	public String tableQ() 
	{
		String s = new String("") ;
		int i=0;
		
		for( Map.Entry<State, Map<Boolean,Float>> entry : tabelaQ.entrySet() )
		{
			i++;
			s += "<" + entry.getKey() + ", " ;
			for( Entry<Boolean, Float> entry2 : entry.getValue().entrySet() )
				s += "<" + entry2.getKey() + ", " + entry2.getValue() + ">" ;

			s += "\n" ;
		}
		
		s += "numero estados= " + i + "\n" ;
		
		return s ;
	}
}
