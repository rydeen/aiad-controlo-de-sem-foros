package aiadproject.ql_algorithm;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;

import aiadproject.Utils.CarQuantity;
import trasmapi.sumo.SumoLane;
import aiadproject.agents.QlearningAgent;

public class State {

	// esquema de cores atual
	public String junctionPhase;
	// quantidade de carros existentes na zona
	public CarQuantity nCars;

	/**
	 * Contrutor por defeito da classe
	 */
	public State() {
		junctionPhase = "";
		nCars = CarQuantity.NONE;
	}

	/**
	 * Contrutor da classe State
	 * @param semaphor
	 */
	public State(QlearningAgent semaphor) {
		junctionPhase = semaphor.getSemaforo().getState();
		nCars = calcNCars( semaphor.getSemaforo().getControlledLanes() ) ;
	}

	/**
	 * Contrutor alternativo para um estado
	 * @param phase
	 * @param controlled_lanes
	 */
	public State( String phase , ArrayList<String> controlled_lanes )
	{
		junctionPhase = phase ;
		nCars = calcNCars(controlled_lanes) ;	
	}
	
	/**
	 * Contrutor alternativo para um estado
	 * @param phase
	 * @param ncars
	 */
	public State( String phase , CarQuantity ncars ) 
	{
		junctionPhase = phase ;
		nCars = ncars ;
	}
	
	/**
	 * M�todo para medir a quantidade de carros numa interse��o
	 * @param controlled_lanes
	 * @return
	 */
	private CarQuantity calcNCars( ArrayList<String> controlled_lanes )
	{
		Set<String> s = new LinkedHashSet<String>(controlled_lanes);
		int Cars = 0;
		for(String id :s)
		{
			SumoLane lane = new SumoLane(id);
			Cars += lane.getNumVehicles();
		}
		
		return CarQuantity.getValue(Cars) ;
	}

	/**
	 * Devolve uma string com as informa��es do objeto atual
	 */
	public String toString() {
		return (new String("{" + junctionPhase + ", " + nCars + "}")) ;
	}
	
	@Override
	public boolean equals(Object obj) 
	{
		if ( !(obj instanceof State) ) 
			return false;

		State estado = (State)obj ;
		
        //System.out.println("------------------------- EQUALS -------------------------") ;
        //System.out.println("this.junctionPhase: " + this.junctionPhase ) ;
        //System.out.println("estado.junctionPhase: " + estado.junctionPhase ) ;
        //System.out.println("equals?? " + (this.junctionPhase).equals(estado.junctionPhase) );
        //System.out.println("this.nCars.name(): " + this.nCars.name() );
        //System.out.println("estado.nCars.name(): " + estado.nCars.name() );
        //System.out.println("equals?? " + this.nCars.name().equals(estado.nCars.name()) );
        //System.out.println("----------------------------------------------------------") ;
        
		if( (this.junctionPhase).equals(estado.junctionPhase) 
				&& this.nCars.name().equals(estado.nCars.name()) )
			return true ;
		
		return false;
	}
	
	@Override
    public int hashCode() 
	{
		//System.out.println("------------------------ HASHCODE ------------------------") ;
		//System.out.println("hash: " + this.junctionPhase.hashCode() + this.nCars.hashCode() );
		//System.out.println("----------------------------------------------------------") ;
        return this.junctionPhase.hashCode() + this.nCars.hashCode();
    }


}
