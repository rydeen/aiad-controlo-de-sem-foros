package aiadproject.Utils;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import trasmapi.sumo.SumoVehicle;


public class Statistics {

	private List<CarStatistics> vehicles;
	int startTime=0;

	public Statistics(int startTime){
		vehicles = new ArrayList<CarStatistics>();
		this.startTime = startTime;
	}

	public void addStatisticalVehicle(SumoVehicle vehicle){
		if(vehicle.departTime >= startTime){
			CarStatistics car = new CarStatistics();
			car.vehicle = vehicle;
			car.id = vehicle.id;
			car.row = vehicles.size();
			vehicles.add(car);
		}
	}

	public void update(int currentStep){
		//update Data
		for (CarStatistics vehicle : vehicles)
		{
			if(vehicle.totalTravelTime == 0 && vehicle.vehicle.lanePosition > 0)
			{
				if(vehicle.vehicle.speed == 0.0 && vehicle.lastStepStopped == 0)
					vehicle.lastStepStopped = currentStep;
				else if(vehicle.vehicle.speed > 0 && vehicle.lastStepStopped > 0)
				{
					vehicle.averageTimeWaited = (vehicle.averageTimeWaited * vehicle.timesStopped + 
							currentStep - vehicle.lastStepStopped) 
							/ ++vehicle.timesStopped;
					vehicle.lastStepStopped = 0;   
				}
				vehicle.sumOfSpeed += vehicle.vehicle.speed;

				if(vehicle.vehicle.arrived)
				{
					vehicle.totalTravelTime = (vehicle.vehicle.arrivalTime - vehicle.vehicle.departTime)/1000f;
					vehicle.averageVelocity = vehicle.sumOfSpeed / (vehicle.totalTravelTime);
					vehicle.averageTimeWaited = vehicle.averageTimeWaited/1000f; 
					vehicle.vehicle = null;
				}
			}
		}
	}

	public void saveStatistics(String path){

		try {
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet("Data");

			//write headers
			HSSFRow rowhead = sheet.createRow(0);
			rowhead.createCell(0).setCellValue("Vehicle ID");
			rowhead.createCell(1).setCellValue("Total Travel Time (s)");
			rowhead.createCell(2).setCellValue("Average Speed (s)");
			rowhead.createCell(3).setCellValue("Average Time Waiting (s)");
			rowhead.createCell(4).setCellValue("Number of Times Stopped");

			//write vehicle information
			CarStatistics vehicle;
			for(int i = 0; i < vehicles.size(); i++)
			{
				vehicle = vehicles.get(i);
				HSSFRow row = sheet.createRow(i+1);
				row.createCell(0).setCellValue(vehicle.id);
				row.createCell(1).setCellValue(vehicle.totalTravelTime);
				row.createCell(2).setCellValue(vehicle.averageVelocity);
				row.createCell(3).setCellValue(vehicle.averageTimeWaited);
				row.createCell(4).setCellValue(vehicle.timesStopped);
			}

			//Averages && Deviations
			//Mean of Total Travel Time
			HSSFRow r1 = sheet.getRow(1);
			r1.createCell(5).setCellValue("Mean of Total Travel Time:");
			r1.createCell(6).setCellFormula("AVERAGE(B2:B"+(vehicles.size()+1)+")");
			//Mean of Average Speed Time
			HSSFRow r2 = sheet.getRow(2);
			r2.createCell(5).setCellValue("Mean of Average Speed Time:");
			r2.createCell(6).setCellFormula("AVERAGE(C2:C"+(vehicles.size()+1)+")");
			//Mean of Average Time Waiting
			HSSFRow r3 = sheet.getRow(3);
			r3.createCell(5).setCellValue("Mean of Average Time Waiting:");
			r3.createCell(6).setCellFormula("AVERAGE(D2:D"+(vehicles.size()+1)+")");
			//Mean of Number of Times Stopped
			HSSFRow r4 = sheet.getRow(4);
			r4.createCell(5).setCellValue("Mean of Number of Times Stopped:");
			r4.createCell(6).setCellFormula("STDEV(E2:E"+(vehicles.size()+1)+")");
			//Deviation of Total Travel Time
			HSSFRow r5 = sheet.getRow(5);
			r5.createCell(5).setCellValue("Deviation of Total Travel Time:");
			r5.createCell(6).setCellFormula("STDEV(B2:B"+(vehicles.size()+1)+")");
			//Deviation of Average Speed Time
			HSSFRow r6 = sheet.getRow(6);
			r6.createCell(5).setCellValue("Deviation of Average Speed Time:");
			r6.createCell(6).setCellFormula("STDEV(C2:C"+(vehicles.size()+1)+")");
			//Deviation of Average Time Waiting
			HSSFRow r7 = sheet.getRow(7);
			r7.createCell(5).setCellValue("Deviation of Average Time Waiting:");
			r7.createCell(6).setCellFormula("STDEV(D2:D"+(vehicles.size()+1)+")");

			//Auto resize for decent display
			sheet.autoSizeColumn(0,false);
			sheet.autoSizeColumn(1,false);
			sheet.autoSizeColumn(2,false);
			sheet.autoSizeColumn(3,false);
			sheet.autoSizeColumn(4,false);
			sheet.autoSizeColumn(5,false);

			File statisticalFile = new File(path);
			if (!statisticalFile.exists())
				statisticalFile.createNewFile();

			FileOutputStream output = new FileOutputStream(statisticalFile, false);
			workbook.write(output);
			output.close();

		}catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
