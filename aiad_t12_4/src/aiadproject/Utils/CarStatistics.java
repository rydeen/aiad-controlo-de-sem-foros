package aiadproject.Utils;

import trasmapi.sumo.SumoVehicle;

public class CarStatistics {
	
	public float       	averageTimeWaited = 0f;
	public double		averageVelocity = 0f;
	public double		sumOfSpeed = 0f;
    public int         	timesStopped = 0;
    public int         	lastStepStopped = 0;
    public float        totalTravelTime = 0f;
    public String 		id;
    public SumoVehicle 	vehicle;
    public int         	row;

}
