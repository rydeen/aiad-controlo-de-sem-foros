package aiadproject.Utils;

public enum CarQuantity {
	NONE(0), FEW(2), SOME(5), MANY(10);
	
	private final int value;
	
	private CarQuantity(int v){
		value = v;
	}
	
	public static CarQuantity getValue(int v){
		CarQuantity found = NONE;
		for(CarQuantity c : values())
			if(c.value <= v)
				found = c ;
		
		return found;
	}
}